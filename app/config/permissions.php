<?php

return array(

    'Globāli' => array(
        array(
            'permission' => 'superuser',
            'label'      => 'Super lietotājs',
        ),
    ),

    'Admin' => array(
        array(
            'permission' => 'admin',
            'label'      => 'Admin tiesības',
        ),
    ),

    //places
    'Vietas' => array(
        array(
            'permission' => 'places.read',
            'label' => 'Vietu apskate'
        ),
        array(
            'permission' => 'places.create',
            'label' => 'Vietu izveide'
        ),
    ),
    //Languages
    'Valodas' => array(
        array(
            'permission' => 'languages.read',
            'label' => 'Valodu apskate'
        ),
        array(
            'permission' => 'languages.create',
            'label' => 'Valodu izveide'
        ),
    ),
    //services
    'Dienesti' => array(
        array(
            'permission' => 'services.read',
            'label' => 'Dienestu apskate'
        ),
        array(
            'permission' => 'services.create',
            'label' => 'Dienestu izveide'
        ),
    ),
    //municipality
    'Pašvaldība' => array(
        array(
            'permission' => 'municipality.read',
            'label' => 'Pašvaldības apskate'
        ),
        array(
            'permission' => 'municipality.create',
            'label' => 'Pašvaldības izveide'
        ),
    ),
    //busstops
    'Pieturas' => array(
        array(
            'permission' => 'busstops.read',
            'label' => 'Pieturu apskate'
        ),
        array(
            'permission' => 'busstops.create',
            'label' => 'Pieturu izveide'
        ),
    ),
    //taxi
    'Taksometri' => array(
        array(
            'permission' => 'taxi.read',
            'label' => 'Taksometru apskate'
        ),
        array(
            'permission' => 'taxi.create',
            'label' => 'Taksometru izveide'
        ),
    ),
    //audioguide
    'Audio gids' => array(
        array(
            'permission' => 'audioguide.read',
            'label' => 'Audio gida apskate'
        ),
        array(
            'permission' => 'audioguide.create',
            'label' => 'Audio gida izveide'
        ),
    ),
    //gallery
    'Galerija' => array(
        array(
            'permission' => 'gallery.read',
            'label' => 'Galeriju apskate'
        ),
        array(
            'permission' => 'gallery.create',
            'label' => 'Galeriju izveide'
        ),
    ),
    //wheretoeat
    'Kur paēst?' => array(
        array(
            'permission' => 'restaurants.read',
            'label' => 'Vietu apskate'
        ),
        array(
            'permission' => 'restaurants.create',
            'label' => 'Vietu izveide'
        ),
    ),
    //hotels
    'Naktsmītnes' => array(
        array(
            'permission' => 'hotels.read',
            'label' => 'Vietu apskate'
        ),
        array(
            'permission' => 'hotels.create',
            'label' => 'Vietu izveide'
        ),
    ),
);
