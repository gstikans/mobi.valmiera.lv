<?php namespace Controllers\Admin;

use AdminController;
use Config;
use Input;
use Lang;
use URL;
use Redirect;
use Sentry;
use Validator;
use View;
use Response;
use Audioguide;
use Place;

class AudioguideController extends AdminController {

	private $audioguides;
	private $uploadDir = "";


	public function __construct(Audioguide $audioguides)
	{
		$this->uploadDir = DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."audioguides".DIRECTORY_SEPARATOR;
		$this->audioguides = $audioguides;
	}

	/*** return main view ***/
	public function getindex()
	{
		//get all audio guides and return view
		$audioList = $this->audioguides->orderBy('order','ASC')->get();
		return View::make('backend/audioguide/index',compact('audioList'));
	}

	/*** return create form ***/
	public function getCreate()
	{
		//get all unassigned places
		$assignedPlaces = $this->audioguides->where('places_id','!=','null')->get();
		$assignedIDList = array();
		if(count($assignedPlaces)>0){
			foreach($assignedPlaces as $audioInfo){
				$assignedIDList[] = $audioInfo->places_id;
			}
		}
		$placeList = (!empty($assignedIDList)) ? Place::whereNotIn('id',$assignedIDList)->get() : Place::all();
		$placeObject = new Place();
		$languageList = $placeObject->languageList;
		return View::make('backend/audioguide/create',compact('placeList','languageList'));
	}

	/*** create new record ***/
	public function postCreate()
	{
		//get data and do validation
		$placeObject = new Place();
		$languageList = $placeObject->languageList;
		//validate files
		$audioguide = new Audioguide;
		if($audioguide->validate(Input::all())){
			if(count($languageList) > 0){
				foreach($languageList as $language){
					if(Input::hasFile('file-'.$language)){
						$fileField = Input::file('file-'.$language);
						$savedFileName = time()."_".cleanFilename($fileField->getClientOriginalName());
						$fileField->move(public_path().$this->uploadDir.$language.DIRECTORY_SEPARATOR,$savedFileName);
						$audioguide->{"file_".$language} = $this->uploadDir.$language.DIRECTORY_SEPARATOR.$savedFileName;
					}
				}
			}
			$audioguide->places_id = (Input::get('places_id')=='0') ? null : e(Input::get('places_id'));
			if($audioguide->save()){
				return Redirect::to('admin/audioguides')->with('success','Audiogida ieraksts veiksmīgi pievienots');
			}
		}else{
			return Redirect::back()->withInput()->withErrors($audioguide->errors());
		}
		return Redirect::to('admin/audioguides')->with('error','Radās problēmas izveidojot audiogida ierakstu');
	}

	/** return records edit view with data ***/
	public function getEdit($id)
	{
		//get and validate record
		if(is_null($audioguide = $this->audioguides->find($id))){
			return Redirect::to('admin/audioguides')->with('error','Nevarēja atrast izvēlēto ierakstu');
		}
		if($audioguide->places_id!=null){
			$assignedPlaces = $this->audioguides->where('places_id','!=','null')->where('places_id','!=',$audioguide->places_id)->get();
		}else{
			$assignedPlaces = $this->audioguides->where('places_id','!=','null')->get();
		}
		$assignedIDList = array();
		if(count($assignedPlaces)>0){
			foreach($assignedPlaces as $audioInfo){
				$assignedIDList[] = $audioInfo->places_id;
			}
		}
		$placeList = (!empty($assignedIDList)) ? Place::whereNotIn('id',$assignedIDList)->get() : Place::all();
		$placeObject = new Place();
		$languageList = $placeObject->languageList;
		return View::make('backend/audioguide/edit',compact('audioguide','languageList','placeList'));
	}

	/*** edit record and redirect ***/
	public function postEdit($id)
	{
		//get data and do validation
		if(is_null($audioguide = $this->audioguides->find($id))){
			return Redirect::to('admin/audioguides')->with('error','Nevarēja atrast izvēlēto ierakstu');
		}
		$placeObject = new Place();
		$languageList = $placeObject->languageList;
		//validation rules
		$rules = [
			'file-lv' => 'max:15360|mimes:mp3,mpga',
			'file-en' => 'max:15360|mimes:mp3,mpga',
			'file-ru' => 'max:15360|mimes:mp3,mpga',
			'file-est' => 'max:15360|mimes:mp3,mpga',
		];
		$audioguide->setValidateRules($rules);
		//validate files
		if($audioguide->validate(Input::all())){
			if(count($languageList) > 0){
				foreach($languageList as $language){
					if(Input::hasFile('file-'.$language)){
						$fileField = Input::file('file-'.$language);
						$savedFileName = time()."_".cleanFilename($fileField->getClientOriginalName());
						$fileField->move(public_path().$this->uploadDir.$language.DIRECTORY_SEPARATOR,$savedFileName);
						$audioguide->{"file_".$language} = $this->uploadDir.$language.DIRECTORY_SEPARATOR.$savedFileName;
					}
				}
			}
			$audioguide->places_id = (Input::get('places_id')=='0') ? null : e(Input::get('places_id'));
			if($audioguide->save()){
				return Redirect::to('admin/audioguides')->with('success','Audiogida ieraksts veiksmīgi pievienots');
			}
		}else{
			return Redirect::back()->withInput()->withErrors($audioguide->errors());
		}
		return Redirect::to('admin/audioguides')->with('error','Radās problēmas izveidojot audiogida ierakstu');
	}

	/*** get modal for file delete ***/
	public function getModalDeleteFile($id,$language)
	{
		$model = "global";
		$confirm_route = $error = null;
		if(is_null($audioguides = $this->audioguides->find($id))){
			$error = 'Nevarēja atrast izvēlēto ierakstu';
			return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
		}
		$confirm_route = URL::action('delete/audioguide-file',array('id'=>$audioguides->id,'language'=>$language));
		return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
	}

	/*** get modal for delete ***/
	public function getModalDelete($id)
	{
		//set data and show modal
		$model = 'global';
		$confirm_route = $error = null;
		if(is_null($audioguides = $this->audioguides->find($id))){
			$error = 'Nevarēja atrast izvēlēto ierakstu';
			return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
		}
		$confirm_route = URL::action('delete/audioguides',array('id'=>$audioguides->id));
		return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
	}

	/*** delete record asset ***/
	public function getDeleteFile($id,$language)
	{
		if(is_null($audioguides = $this->audioguides->find($id)))
		{
			return Redirect::to('admin/audioguides')->with('error','Nevarēja atrasts izvēlēto ierakstu');
		}
		$audioguides->fileDelete($language);
		return Redirect::to('admin/audioguides')->with('success','Fails veiksmīgi izdzēsts');
	}

	/*** delete record ***/
	public function getDelete($id)
	{
		//validate data and remove record
		if(is_null($audioguides = $this->audioguides->find($id))){
			return Redirect::to('admin/audioguides')->with('error','Nevarēja atrast ierakstu');
		}
		$audioguides->delete();
		return Redirect::to('admin/audioguides')->with('success','Ieraksts veiksmīgi izdzēsts');
	}

	/*** get json response ***/
	public function getJsonList()
	{
		//gather data and return as json
		$audioGuides = [];
		$audioList = $this->audioguides->orderBy('id','ASC')->get();
		if(count($audioList) > 0){
			foreach($audioList as $audioInfo){
				$audioGuidePlace = $audioInfo->audioPlace;
				$audioGuides[] = [
					'file_lv' => ($audioInfo->file_lv!==null) ? URL::to('/').$audioInfo->file_lv : '',
					'file_en' => ($audioInfo->file_en!==null) ? URL::to('/').$audioInfo->file_en : '',
					'file_ru' => ($audioInfo->file_ru!==null) ? URL::to('/').$audioInfo->file_ru : '',
					'file_est' => ($audioInfo->file_est!==null) ? URL::to('/').$audioInfo->file_est : '',
					'placeInfo' => ($audioGuidePlace !== null) ? $audioGuidePlace->toArray() : null
				];
			}
		}
		return Response::json(array('Items'=>$audioGuides));
	}		

}