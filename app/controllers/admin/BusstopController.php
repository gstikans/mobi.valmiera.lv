<?php namespace Controllers\Admin;

use AdminController;
use Config;
use Input;
use Lang;
use URL;
use Redirect;
use Sentry;
use Validator;
use View;
use Response;
use Busstop;

class BusstopController extends AdminController {

	private $busstops;
	private $uploadDir = "";

	public function __construct(Busstop $busstops)
	{
		$this->busstops = $busstops;
		$this->uploadDir = DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."busstops".DIRECTORY_SEPARATOR;
	}

	/*** return main view ***/
	public function getindex()
	{
		//get complete busstop list ordered by name
		$busstopList = $this->busstops->orderBy('name','ASC')->get();
		return View::make('backend/busstop/index',compact('busstopList'));
	}

	/*** return main map view ***/
	public function getMapView()
	{
		$busstopList = $this->busstops->orderBy('name','ASC')->get();
		return View::make('backend/busstop/map',compact('busstopList'));
	}

	/*** return create view ***/
	public function getCreate()
	{
		//return create view
		return View::make('backend/busstop/create');
	}

	/*** create new record ***/
	public function postcreate()
	{
		//get and validate data
		$busstop = new Busstop;
		if($busstop->validate(Input::all())){
			$busstop->name = e(Input::get('name'));
			$busstop->latlng = e(str_replace(")","",str_replace("(","",Input::get('latlng'))));
			$busstop->file = "";
			//handle file upload
			if(Input::hasFile('file') && Input::file('file')->getMimeType() == "application/pdf"){
				$file = Input::file('file');
				$savedFileName = time()."_".$file->getClientOriginalName();
				$file->move(public_path().$this->uploadDir,$savedFileName);
				//determine type and convert if possible
				if(file_exists(public_path().$this->uploadDir.$savedFileName) && class_exists('Imagick')){
					$image = new \Imagick();
					$image->setResolution(100,100);
					$image->readImage(public_path().$this->uploadDir.$savedFileName);
					$image->resetIterator();
					$image = $image->appendImages(true);
					$image->setImageFormat('jpeg');
					$image->setImageCompression(\Imagick::COMPRESSION_JPEG);
					$image->setImageCompressionQuality(90);
					$image->writeImage(public_path().$this->uploadDir.substr($savedFileName,0,-4).".jpg");
					$image->clear();
					$image->destroy();
					//set database
					$busstop->file = $this->uploadDir.substr($savedFileName,0,-4).".jpg";
					//delete uploaded file 
					unlink(public_path().$this->uploadDir.$savedFileName);
				}
				$busstop->file = $this->uploadDir.$savedFileName;
			}
			if($busstop->save()){
				return Redirect::to('admin/busstops')->with('success','Pieturas ieraksts veiksmīgi izveidots');
			}
		}else{
			return Redirect::back()->withInput()->withErrors($busstop->errors());
		}
		return Redirect::to('admin/busstops')->with('error','Nevarēja izveidot pieturas ierakstu');
	}

	/** return records edit view with data ***/
	public function getEdit($id)
	{
		//get data
		if(is_null($busstop = $this->busstops->find($id))){
			return Redirect::to('admin/busstops')->with('error','Nevarēja atrast izvēlēto ierakstu');
		}
		return View::make('backend/busstop/edit',compact('busstop'));
	}

	/*** edit record and redirect ***/
	public function postEdit($id)
	{
		//get data and validate
		if(is_null($busstop = $this->busstops->find($id))){
			return Redirect::to('admin/busstops')->with('error','Nevarēja atrast izvēlēto ierakstu');
		}
		//validate against model
		if($busstop->validate(Input::all())){
			$busstop->name = e(Input::get('name'));
			$busstop->latlng = e(str_replace(")","",str_replace("(","",Input::get('latlng'))));
			//deal with file
			if(Input::hasFile('file') && Input::file('file')->getMimeType() == "application/pdf"){
				$file = Input::file('file');
				$savedFileName = time()."_".$file->getClientOriginalName();
				$file->move(public_path().$this->uploadDir,$savedFileName);
				if(file_exists(public_path().$this->uploadDir.$savedFileName) && class_exists('Imagick')){
					$image = new \Imagick();
					$image->setResolution(100,100);
					$image->readImage(public_path().$this->uploadDir.$savedFileName);
					$image->resetIterator();
					$image = $image->appendImages(true);
					$image->setImageFormat('jpeg');
					$image->setImageCompression(\Imagick::COMPRESSION_JPEG);
					$image->setImageCompressionQuality(90);
					$image->writeImage(public_path().$this->uploadDir.substr($savedFileName,0,-4).".jpg");
					$image->clear();
					$image->destroy();
					//remove old file
					$busstop->fileDelete();
					//set to database
					$busstop->file = $this->uploadDir.substr($savedFileName,0,-4).".jpg";
					//delete uploaded file 
					unlink(public_path().$this->uploadDir.$savedFileName);
				}else{
					//remove old file
					$busstop->fileDelete();
					//add new file to db record
					$busstop->file = $this->uploadDir.$savedFileName;
				}
			}
			if($busstop->save()){
				return Redirect::to('admin/busstops')->with('success','Ieraksts veiksmīgi atjaunināts');
			}
		}else{
			return Redirect::back()->withInput()->withErrors($busstop->errors());
		}
		return Redirect::to('admin/busstops')->with('error','Nevarēja atjaunināt izvēlēto ierakstu');
	}

	/*** get modal for delete ***/
	public function getModalDelete($id)
	{
		//set data and show modal
		$model = 'global';
		$confirm_route = $error = null;
		if(is_null($busstops = $this->busstops->find($id))){
			$error = 'Nevarēja atrast izvēlēto ierakstu';
			return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
		}
		$confirm_route = URL::action('delete/busstop',array('id'=>$busstops->id));
		return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
	}

	/*** delete record ***/
	public function getDelete($id)
	{
		//validate data and remove record
		if(is_null($busstops = $this->busstops->find($id))){
			return Redirect::to('admin/busstops')->with('error','Nevarēja atrast ierakstu');
		}
		$busstops->delete();
		return Redirect::to('admin/busstops')->with('success','Ieraksts veiksmīgi izdzēsts');
	}

	/*** get Delete file ***/
	public function getDeleteFile($id){
		if(is_null($busstops = $this->busstops->find($id))){
			return Redirect::to('admin/busstops')->with('error','Nevarēja atrast ierakstu');
		}
		$busstops->fileDelete();
		return Redirect::to('admin/busstops')->with('success','Pieturas fails veiksmīgi izdzēsts');
	}

	/*** get json response ***/
	public function getJsonList()
	{
		//gather data and return as json
		$busstops = array();
		$nameArray = [];
		$directionList = [];
		$busstopList = $this->busstops->orderBy('name','ASC')->get();
		if(count($busstopList) > 0){
			$i=0;
			foreach($busstopList as $busstopInfo){
				$busstops[$i] = array(
					'name' => html_entity_decode($busstopInfo->name,0,'UTF-8'),
					'latlng' => $busstopInfo->latlng,
					'file' => ($busstopInfo->file!=="") ? URL::to('/').$busstopInfo->file : '',
					'fileName' => ($busstopInfo->file!=="") ? $busstopInfo->fileName() : ''
				);
				//split name by ( and -
				$nameFix = str_replace("-","(",$busstopInfo->name);
				$nameParts = explode("(", $nameFix);
				if(count($nameParts)>1){
					$namePartFix = str_replace(")","",$nameParts[1]);
					$namePartFix = str_replace("_"," ",$namePartFix);
					$nameArray["".html_entity_decode($namePartFix,0,'UTF-8').""][] = $busstops[$i];
				}
			$i++;}
		}
		$directionList = array_keys($nameArray);
		//return Response::json(array('Items'=>$nameArray));
		return Response::json(array('Items'=>$busstops,'byDirection'=>$nameArray,'directionList'=>$directionList));
	}

}