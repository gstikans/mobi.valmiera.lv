<?php namespace Controllers\Admin;

use AdminController;
use Config;
use Input;
use Lang;
use URL;
use Redirect;
use Sentry;
use Validator;
use View;
use Response;
use Hotel;
use Geocoder;

class HotelController extends AdminController {

	private $hotels;

	public function __construct(Hotel $hotels)
	{
		$this->hotels = $hotels;
	}

	/*** return main view ***/
	public function getindex()
	{
		//get all hotels ordered by name
		$hotelList = $this->hotels->orderBy('name','ASC')->get();
		return View::make('backend/hotels/index',compact('hotelList'));
	}

	/*** create new record ***/
	public function postcreate()
	{
		//get and validate data
		$hotel = new Hotel;
		if($hotel->validate(Input::all())){
			$hotel->name = e(Input::get('name'));
			$hotel->address = e(Input::get('address'));
			$hotel->phone = e(Input::get('phone'));
			$hotel->mail = e(Input::get('mail'));
			$hotel->web = e(Input::get('web'));
			try {
				$hotelGeocode = Geocoder::geocode(e(Input::get('address')));
				$hotel->latlng = $hotelGeocode->getLatitude().",".$hotelGeocode->getLongitude();
			}catch(Exception $e){
				$hotel->latlng = null;
			}
			if($hotel->save()){
				return Redirect::to('admin/hotels')->with('success','Naktsmītnes ieraksts veiksmīgi pievienots');
			}
		}else{
			return Redirect::back()->withInput()->withErrors($hotel->errors());
		}
		return Redirect::to('admin/hotels')->with('error','Nevarēja izviedot naktsmītnes ierakstu');
	}

	/** return records edit view with data ***/
	public function getEdit($id)
	{
		//get data
		if(is_null($hotel = $this->hotels->find($id))){
			return Redirect::to('admin/hotels')->with('error','Nevarēja atrast jūsu izvēlēto ierakstu');
		}
		return View::make('backend/hotels/edit',compact('hotel'));
	}

	/*** edit record and redirect ***/
	public function postEdit($id)
	{
		//get data and validate
		if(is_null($hotel = $this->hotels->find($id))){
			return Redirect::to('admin/hotels')->with('error','Nevarēja atrast jūsu izvēlēto ierakstu');
		}
		$rules = array(
			'name' => "required|min:3|unique:hotels,id,$id",
			'address' => 'required|min:3',
			'mail' => 'email',
			'web' => 'url'
		);
		$validator = Validator::make(Input::all(),$rules);
		if(!$validator->fails()){
			$hotel->name = e(Input::get('name'));
			$hotel->address = e(Input::get('address'));
			$hotel->phone = e(Input::get('phone'));
			$hotel->mail = e(Input::get('mail'));
			$hotel->web = e(Input::get('web'));
			try {
				$hotelGeocode = Geocoder::geocode(e(Input::get('address')));
				$hotel->latlng = $hotelGeocode->getLatitude().",".$hotelGeocode->getLongitude();
			}catch(Exception $e){
				$retaurant->latlng = null;
			}
			if($hotel->save()){
				return Redirect::to('admin/hotels')->with('success','Vietas ieraksts veiksmīgi atjaunots');
			}
		}else{
			return Redirect::back()->withInput()->withErrors($validator);
		}
		return Redirect::to('admin/hotels')->with('error','Nevarēja atrast jūsu izvēlēto ierakstu');
	}

	/*** get modal for delete ***/
	public function getModalDelete($id)
	{
		//set data and show modal
		$model = 'global';
		$confirm_route = $error = null;
		if(is_null($hotels = $this->hotels->find($id))){
			$error = 'Nevarēja atrast izvēlēto ierakstu';
			return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
		}
		$confirm_route = URL::action('delete/hotel',array('id'=>$hotels->id));
		return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
	}

	/*** delete record ***/
	public function getDelete($id)
	{
		//validate data and remove record
		if(is_null($hotels = $this->hotels->find($id))){
			return Redirect::to('admin/hotels')->with('error','Nevarēja atrast ierakstu');
		}
		$hotels->delete();
		return Redirect::to('admin/hotels')->with('success','Ieraksts veiksmīgi izdzēsts');
	}

	/*** get json response ***/
	public function getJsonList()
	{
		//gather data and return as json
		$hotels = array();
		$hotelList = $this->hotels->orderBy('name','ASC')->get();
		if(count($hotelList) > 0){
			foreach($hotelList as $hotelInfo){
				$hotels[] = array(
					'name' => html_entity_decode($hotelInfo->name,0,'UTF-8'),
					'address' => html_entity_decode($hotelInfo->address,0,'UTF-8'),
					'mail' => $hotelInfo->mail,
					'phone' => $hotelInfo->phone,
					'web' => $hotelInfo->web,
					'latlng' => $hotelInfo->latlng
				);
			}
		}
		return Response::json(array('Items'=>$hotels));
	}

}