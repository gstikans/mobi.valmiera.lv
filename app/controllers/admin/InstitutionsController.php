<?php namespace Controllers\Admin;

use AdminController;
use Config;
use Input;
use Lang;
use URL;
use Redirect;
use Sentry;
use Validator;
use View;
use Response;
use Institution;
use InstitutionUser;

class InstitutionsController extends AdminController
{

    protected $validationRules = array(
        'name' => 'required|min:3|unique:institutions',
    );

    protected $institution;
    protected $institutionUser;

      public function __construct(Institution $institution, InstitutionUser $institutionUser)
      {
          $this->institution = $institution;
          $this->institutionUser = $institutionUser;
      }

    /**
     * Show a list of all institutions and users.
     *
     * @return View
     */
    public function getIndex()
    {
        // Grab all the institutions;
        $institutions = $this->institution->orderBy('weight','ASC')->get();
        // Show the page
        return View::make('backend/institutions/index', compact('institutions'));
    }

    /**
    * try and create institutions
    *
    * @return Redirect
    */
    public function postCreate()
    {
        //create new institution
        $institution = new Institution;
        $lastInstitution = new Institution;
        $weight = $lastInstitution->max('weight');
        if($institution->validate(Input::all())){
          $institution->name = e(Input::get('name'));
          $institution->weight = ($weight+1);
          if($institution->save()){
            return Redirect::to('admin/institutions')->with('success','Struktūrvienības ieraksts veiksmīgi izveidots');
          }
        }else{
          return Redirect::back()->withInput()->withErrors($institution->errors());
        }
        return Redirect::to('admin/institutions')->with('error','Nevarēja izveidot struktūrvienības ierakstu');
    }

    /*** get Edit ***/
    public function getEdit($institutionID = null)
    {
      if(is_null($institution = $this->institution->find($institutionID))){
        return Redirect::to('admin/institutions')->with('error','Šāds struktūrvienības ieraksts neeksistē');
      }
      return View::make('backend/institutions/edit',compact('institution'));
    }

    /*** post edit ***/
    public function postEdit($institutionID = null)
    {
      if(is_null($institution = $this->institution->find($institutionID))){
        return Redirect::to('admin/institutions')->with('error','Šāds struktūrvienības ieraksts neeksistē');
      }
      //validation rules
      $rules = array(
        'name' => 'required|min:3|unique:institutions'
      );
      $validator = Validator::make(Input::all(),$rules);
      if($validator->fails()){
        return Redirect::back()->withInput()->withErrors($validator);
      }
      //update institution record
      $institution->name = e(Input::get('name'));
      if($institution->save()){
        return Redirect::to('admin/institutions')->with('success','Struktūrvienības ieraksts veiksmīgi atjaunināts');
      }
      return Redirect::to('admin/institutions')->with('error','Nevarēja atjaunināt struktūrvienības ierakstu');
    }

    /*** get modal delete ***/
    public function getModalDelete($institutionID = null){
      $model = 'institutions';
      $confirm_route = $error = null;
      if(is_null($institution = $this->institution->find($institutionID))){
        $error = 'Nevarēja atrast izvēlēto struktūrvienības ierakstu';
        return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
      }
      $confirm_route = URL::action('delete/institution',array('id'=>$institution->id));
      return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
    }

    /*** get delete ***/
    public function getDelete($institutionID)
    {
      if(is_null($institution = $this->institution->find($institutionID))){
        return Redirect::to('admin/institutions')->with('errors','Struktūrvienības ieraksts netika atrasts');
      }
      $institution->delete();

      return Redirect::to('admin/institutions')->with('success','Struktūrvienības ieraksts veiksmīgi izdzēsts');
    }

    /* INSTITUTION WEIGHT */
    public function postInstitutionWeight()
    {
      $institutionList = Input::get('institution');
      if(isset($institutionList) && count($institutionList) > 0){
        foreach($institutionList as $weight=>$institutionID){
          $institution = $this->institution->find($institutionID);
          $institution->weight = ($weight+1);
          $institution->save();
        }
        return Response::json(array('success'=>true));
      }
    }

    // INSTITUTION USERS //
    /* PERSON WEIGHT */
    public function postPersonWeight()
    {
      $personList = Input::get('user');
      if(isset($personList) && count($personList) > 0){
        foreach($personList as $weight=>$personID){
          $person = $this->institutionUser->find($personID);
          $person->weight = ($weight+1);
          $person->save();
        }
        return Response::json(array('success'=>true));
      }
    }

    /*** get use list ***/
    public function getUserList($institutionID)
    {
      if(is_null($institution = $this->institution->find($institutionID))){
        return Redirect::to('admin/institutions')->with('error','Struktūrvienības ieraksts netika atrasts');
      }
      //get all institutions
      $institutionList = $this->institution->orderBy('weight','ASC')->get();
      $userList = $institution->institutionUsers;
      return View::make('backend/institutions/user_index',compact('userList','institution','institutionList'));
    }

    /*** create user ***/
    public function postUser($institutionID){
      if(is_null($institution = $this->institution->find($institutionID))){
        return Redirect::to('admin/institutions')->with('error','Nevarēja atrast šādu struktūrvienību');
      }
      //get data
      $institutionUser = new InstitutionUser;
      $lastInstitutionUser = new InstitutionUser;
      $weight = $lastInstitutionUser->where('institution_id','=',$institution->id)->max('weight');
      Input::merge(array('institution_id'=>$institution->id));
      if($institutionUser->validate(Input::all())){
        //make post
        $institutionUser->name = e(Input::get('name'));
        $institutionUser->job = e(Input::get('job'));
        $institutionUser->mail = e(Input::get('mail'));
        $institutionUser->phone = e(Input::get('phone'));
        $institutionUser->institution_id = $institution->id;
        $institutionUser->weight = ($weight+1);
        if($institutionUser->save()){
          return Redirect::to('admin/institutions/'.$institution->id.'/users')->with('success','Personas ieraksts veiksmīgi izveidots');
        }
      }else{
        return Redirect::back()->withInput()->withErrors($institutionUser->errors());
      }
      return Redirect::to('admin/institutions/'.$institution->id.'/users')->with('error','Nevarēja izveidot personas ierakstu');
    }

    /*** get user edit form ***/
    public function getUserEdit($institutionID,$userID)
    {
      if(is_null($institution = $this->institution->find($institutionID))){
        return Redirect::to('admin/institutions')->with('error','Nevarēja atrast attiecīgo sturktūrvienību');
      }
      if(is_null($user = $this->institutionUser->find($userID))){
        return Redirect::to('admin/institutions')->with('error','Nevarēja atrast attiecīgo personu');
      }
      $institutionList = $this->institution->orderBy('weight','ASC')->get();
      return View::make('backend/institutions/user_edit',compact('institution','user','institutionList'));
    }

    /*** post user edit ***/
    public function postUserEdit($institutionID,$userID)
    {
      if(is_null($institution = $this->institution->find($institutionID))){
        return Redirect::to('admin/institutions')->with('error','Nevarēja atrast izvēlēto struktūrvienību');
      }
      if(is_null($user = $this->institutionUser->find($userID))){
        return Redirect::to('admin/institutions')->with('error','Nevarēja atrast personas ierakstu');
      }
      $rules = array(
        'name' => "required|min:3|unique:institution_users,id,$institutionID",
        'institution_id' => 'required',
      );
      Input::merge(array('institution_id'=>$institution->id));
      if(!Validator::make(Input::all(),$rules)->fails()){
        $user->name = e(Input::get('name'));
        $user->job = e(Input::get('job'));
        $user->phone = e(Input::get('phone'));
        $user->mail = e(Input::get('mail'));
        if($user->save()){
          return Redirect::to('admin/institutions/'.$institution->id.'/users')->with('success','Struktūrvienības personas ieraksts veiksmīgi atjaunināts');
        }
      }else{
        return Redirect::back()->withInput()->withErrors($user->errors());
      }
      return Redirect::to('admin/institutions')->with('error','Nevarēja atrast izvēlēto personas ierakstu');
    }

    /*** get modal delete ***/
    public function getModalDeleteUser($institutionID = null, $userID = null){
      $model = 'institutions';
      $confirm_route = $error = null;
      if(is_null($institution = $this->institution->find($institutionID))){
        $error = 'Nevarēja atrast izvēlēto struktūrvienības ierakstu';
        return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
      }
      if(is_null($user = $this->institutionUser->find($userID))){
        $error = 'Nevarēja atrast izvēlēto personas ierakstu';
        return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
      }
      $confirm_route = URL::action('delete/institutionUser',array('institutionID'=>$institution->id,'userID'=>$user->id));
      return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
    }

    /*** get delete ***/
    public function getDeleteUser($institutionID,$userID)
    {
      if(is_null($institution = $this->institution->find($institutionID))){
        return Redirect::to('admin/institutions')->with('errors','Struktūrvienības ieraksts netika atrasts');
      }
      if(is_null($user = $this->institutionUser->find($userID))){
        return Redirect::to('admin/institutions')->with('errors','Struktūrvienības personas ieraksts netika atrasts');
      }
      $user->delete();

      return Redirect::to('admin/institutions/'.$institution->id.'/users')->with('success','Struktūrvienības personas ieraksts veiksmīgi izdzēsts');
    }

    /*** GET JSON LIST ***/
    public function getInstitutionsJSON()
    {
      //get all institutions
      $institutionList = $this->institution->orderBy('weight','ASC')->get();
      $institutions = array();
      $institutionNameList = [];
     //$institutionList = array();
      if(count($institutionList) > 0){
        foreach($institutionList as $institution){
          $institutionName = html_entity_decode($institution->name,0,'UTF-8');
          $institutions[$institutionName] = array(
            'name' => $institutionName,
            'users' => $institution->institutionUsers->toArray()
          );
          $institutionNameList[] = $institutionName;
        }
      }
      return Response::json(array('Items'=>$institutions,'institutionList'=>$institutionNameList));
    }

}