<?php namespace Controllers\Admin;

use AdminController;
use Config;
use Input;
use Lang;
use URL;
use Redirect;
use Sentry;
use Validator;
use View;
use Response;
use Place;

class PlaceController extends AdminController {

	private $places;

	public function __construct(Place $places)
	{
		$this->places = $places;
	}

	/*** return main view ***/
	public function getindex()
	{
		//get all places
		$placeList = $this->places->orderBy('order','ASC')->get();
		return View::make('backend/places/index',compact('placeList'));
	}

	/*** return map view ***/
	public function getMapView()
	{
		//get all places
		$placeList = $this->places->orderBy('order','ASC')->get();
		return View::make('backend/places/map',compact('placeList'));
	}

	/*** return create form ***/
	public function getCreate()
	{
		//return view
		$languageList = $this->places->languageList;
		return View::make('backend/places/create',compact('languageList'));
	}

	/*** create new record ***/
	public function postcreate()
	{
		//get and validate data
		$place = new Place;
		$order = $this->places->max('order');
		if($place->validate(Input::all())){
			$place->latlng = e(str_replace("(","",str_replace(")","",Input::get('latlng'))));
			foreach($this->places->languageList as $language){
				$place->{"name_".$language} = e(Input::get('name_'.$language));
				$place->{"about_".$language} = e(Input::get('about_'.$language));
			}
			//set order
			$place->order = ($order+1);
			if($place->save()){
				return Redirect::to('admin/places')->with('success','Ieraksts veiksmīgi izveidots');
			}
		}else{
			return Redirect::back()->withInput()->withErrors($place->errors());
		}
		return Redirect::to('admin/places')->with('error','Nevarēja atrast izvēlēto ierakstu');
	}

	/** return records edit view with data ***/
	public function getEdit($id)
	{
		//get and validate
		if(is_null($place = $this->places->find($id))){
			return Redirect::to('admin/places')->with('error','Nevarēja atrast izvēlēto ierakstu');
		}
		$languageList = $this->places->languageList;
		return View::make('backend/places/edit',compact('place','languageList'));
	}

	/*** edit record and redirect ***/
	public function postEdit($id)
	{
		//get and validate data
		if(is_null($place = $this->places->find($id))){
			return Redirect::to('admin/places')->with('error','Nevarēja atrast izvēlēto ierakstu');
		}
		if($place->validate(Input::all())){
			$place->latlng = e(str_replace("(","",str_replace(")","",Input::get('latlng'))));
			//language updates
			foreach($this->places->languageList as $language){
				$place->{"name_".$language} = e(Input::get('name_'.$language));
				$place->{"about_".$language} = e(Input::get('about_'.$language));
			}
			if($place->save()){
				return Redirect::to('admin/places')->with('success','Ieraksts veiksmīgi atjaunināts');
			}
		}else{
			return Redirect::back()->withInput()->withErrors($place->errors());
		}
		return Redirect::to('admin/places')->with('error','Nevarēja atrast izvēlēto ierakstu');
	}

	/*** get modal for delete ***/
	public function getModalDelete($id)
	{
		//set data and show modal
		$model = 'global';
		$confirm_route = $error = null;
		if(is_null($places = $this->places->find($id))){
			$error = 'Nevarēja atrast izvēlēto ierakstu';
			return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
		}
		$confirm_route = URL::action('delete/place',array('id'=>$places->id));
		return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
	}

	/*** delete record ***/
	public function getDelete($id)
	{
		//validate data and remove record
		if(is_null($places = $this->places->find($id))){
			return Redirect::to('admin/places')->with('error','Nevarēja atrast ierakstu');
		}
		$places->delete();
		return Redirect::to('admin/places')->with('success','Ieraksts veiksmīgi izdzēsts');
	}

	/*** post place order ***/
	public function postOrder()
	{
		$placeList = Input::get('place');
		if(isset($placeList) && count($placeList) > 0){
			foreach($placeList as $order=>$placeID){
				$place = $this->places->find($placeID);
				$place->order = ($order+1);
				$place->save();
			}
			return Response::json(array('success'=>true));
		}
	}

	/*** get json response ***/
	public function getJsonList()
	{
		//
	}

}