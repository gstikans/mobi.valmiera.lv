<?php namespace Controllers\Admin;

use AdminController;
use Config;
use Input;
use Lang;
use URL;
use Redirect;
use Sentry;
use Validator;
use View;
use Response;
use Restaurant;
use Geocoder;

class RestaurantController extends AdminController {

	protected $restaurants;

	public function __construct(Restaurant $restaurants)
	{
		$this->restaurants = $restaurants;
	}

	/*** return main view ***/
	public function getindex()
	{
		//get all restaurants
		$restaurantList = $this->restaurants->orderBy('name','ASC')->get();
		return View::make('backend/restaurants/index',compact('restaurantList'));
	}

	/*** create new record ***/
	public function postcreate()
	{
		//get and validate data
		$restaurant = new Restaurant;
		if($restaurant->validate(Input::all())){
			$restaurant->name = e(Input::get('name'));
			$restaurant->address = e(Input::get('address'));
			$restaurant->phone = e(Input::get('phone'));
			$restaurant->mail = e(Input::get('mail'));
			$restaurant->web = e(Input::get('web'));
			try {
				$restaurantGeocode = Geocoder::geocode(e(Input::get('address')));
				$restaurant->latlng = $restaurantGeocode->getLatitude().",".$restaurantGeocode->getLongitude();
			}catch(Exception $e){
				$restaurant->latlng = null;
			}
			if($restaurant->save()){
				return Redirect::to('admin/restaurants')->with('success','Vietas ieraksts veiksmīgi pievienots');
			}
		}else{
			return Redirect::back()->withInput()->withErrors($restaurant->errors());
		}
		return Redirect::to('admin/restaurants')->with('error','Nevarēja izviedot vietas ierakstu');
	}

	/** return records edit view with data ***/
	public function getEdit($id)
	{
		//get data
		if(is_null($restaurant = $this->restaurants->find($id))){
			return Redirect::to('admin/restaurants')->with('error','Nevarēja atrast jūsu izvēlēto ierakstu');
		}
		return View::make('backend/restaurants/edit',compact('restaurant'));
	}

	/*** edit record and redirect ***/
	public function postEdit($id)
	{
		//get data and validate
		if(is_null($restaurant = $this->restaurants->find($id))){
			return Redirect::to('admin/restaurants')->with('error','Nevarēja atrast jūsu izvēlēto ierakstu');
		}
		$rules = array(
			'name' => "required|min:3|unique:restaurants,id,$id",
			'address' => 'required|min:3',
			'mail' => 'email',
			'web' => 'url'
		);
		$validator = Validator::make(Input::all(),$rules);
		if(!$validator->fails()){
			$restaurant->name = e(Input::get('name'));
			$restaurant->address = e(Input::get('address'));
			$restaurant->phone = e(Input::get('phone'));
			$restaurant->mail = e(Input::get('mail'));
			$restaurant->web = e(Input::get('web'));
			try {
				$restaurantGeocode = Geocoder::geocode(e(Input::get('address')));
				$restaurant->latlng = $restaurantGeocode->getLatitude().",".$restaurantGeocode->getLongitude();
			}catch(Exception $e){
				$retaurant->latlng = null;
			}
			if($restaurant->save()){
				return Redirect::to('admin/restaurants')->with('success','Vietas ieraksts veiksmīgi atjaunots');
			}
		}else{
			return Redirect::back()->withInput()->withErrors($validator);
		}
		return Redirect::to('admin/restaurants')->with('error','Nevarēja atrast jūsu izvēlēto ierakstu');
	}

	/*** get modal for delete ***/
	public function getModalDelete($id)
	{
		//set data and show modal
		$model = 'global';
		$confirm_route = $error = null;
		if(is_null($restaurants = $this->restaurants->find($id))){
			$error = 'Nevarēja atrast izvēlēto ierakstu';
			return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
		}
		$confirm_route = URL::action('delete/restaurant',array('id'=>$restaurants->id));
		return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
	}

	/*** delete record ***/
	public function getDelete($id)
	{
		//validate data and remove record
		if(is_null($restaurants = $this->restaurants->find($id))){
			return Redirect::to('admin/restaurants')->with('error','Nevarēja atrast ierakstu');
		}
		$restaurants->delete();
		return Redirect::to('admin/restaurants')->with('success','Ieraksts veiksmīgi izdzēsts');
	}

	/*** get json response ***/
	public function getJsonList()
	{
		//gather data and return as json
		$restaurants = array();
		$restaurantList = $this->restaurants->orderBy('name','ASC')->get();
		if(count($restaurantList) > 0){
			foreach($restaurantList as $restaurantInfo){
				$restaurants[] = array(
					'name' => html_entity_decode($restaurantInfo->name,0,'UTF-8'),
					'address' => html_entity_decode($restaurantInfo->address,0,'UTF-8'),
					'mail' => $restaurantInfo->mail,
					'phone' => $restaurantInfo->phone,
					'web' => $restaurantInfo->web,
					'latlng' => $restaurantInfo->latlng
				);
			}
		}
		return Response::json(array('Items'=>$restaurants));
	}
}