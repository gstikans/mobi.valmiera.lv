<?php namespace Controllers\Admin;

use AdminController;
use Config;
use Input;
use Lang;
use URL;
use Redirect;
use Sentry;
use Validator;
use View;
use Service;
use Response;
use Geocoder;

class ServicesController extends AdminController
{

    protected $validationRules = array(
        'name' => 'required|min:3|unique:services',
        'address'   => "required|min:3",
        'phone' => 'min:8'
    );

    protected $service;

      public function __construct(Service $service)
      {
          $this->service = $service;
      }

    /**
     * Show a list of all the languages.
     *
     * @return View
     */
    public function getIndex()
    {
        // Grab all services
        $services = $this->service->orderBy('name','ASC')->paginate(10);
        // Show the page
        return View::make('backend/services/index',compact('services'));
    }

    /**
    * try and create language
    *
    * @return Redirect
    */
    public function postCreate()
    {
        //create object
        $service = new Service;
        if($service->validate(Input::all())){

          //try geocoder
          try {
              $geocode = Geocoder::geocode(e(Input::get('address')));
              $service->latlng = $geocode->getLatitude().",".$geocode->getLongitude();
          }
          catch (Exception $e){
              $service->latlng = null;
          }

          $service->name = e(Input::get('name'));
          $service->address = e(Input::get('address'));
          $service->phone = e(Input::get('phone'));
          $service->mail = e(Input::get('mail'));
          $service->description = e(Input::get('description'));
          if($service->save()){
            return Redirect::to('admin/services')->with('success','Dienesta ieraksts veiksmīgi pievienots');
          }
        }else{
          return Redirect::back()->withInput()->withErrors($service->errors());
        }
        return Redirect::to('admin/services')->with('error','Nevarēja izveidot dienesta ierakstu');
    }

    /*** get Edit ***/
    public function getEdit($serviceID = null)
    {
      if(is_null($service = $this->service->find($serviceID))){
        return Redirect::to('admin/services')->with('error','Šāds dienesta ieraksts neeksistē');
      }
      return View::make('backend/services/edit',compact('service'));
    }

    /*** post edit ***/
    public function postEdit($serviceID = null)
    {
      if(is_null($service = $this->service->find($serviceID))){
        return Redirect::to('admin/services')->with('error','Šāds dienesta ieraksts neeksistē');
      }
      //validation rules
      $rules = array(
        'name' => "required|min:3|unique:services,id,".$serviceID,
        'address' => "required|min:3",
        'phone' => "min:8"
      );
      $validator = Validator::make(Input::all(),$rules);
      if($validator->fails()){
        return Redirect::back()->withInput()->withErrors($validator);     
      }

      //try geocoder
      try {
          $geocode = Geocoder::geocode(e(Input::get('address')));
          $service->latlng = $geocode->getLatitude().",".$geocode->getLongitude();
      }
      catch (Exception $e){
          $service->latlng = null;
      }

      //update
      $service->name = e(Input::get('name'));
      $service->address = e(Input::get('address'));
      $service->phone = e(Input::get('phone'));
      $service->mail = e(Input::get('mail'));
      $service->description = e(Input::get('description'));
      if($service->save()){
        return Redirect::to('admin/services')->with('success','Dienesta ieraksts veiksmīgi atjaunināts');
      }
      return Redirect::to('admin/services')->with('error','Nevarēja atjaunināt dienesta ierakstu');
    }

    /*** get modal delete ***/
    public function getModalDelete($service = null){
      $model = 'service';
      $confirm_route = $error = null;
      if(is_null($service = $this->service->find($serviceID))){
        $error = 'Nevarēja atrast izvēlēto dienesta ierakstu';
        return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
      }
      $confirm_route = URL::action('delete/service',array('id'=>$service->id));
      return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
    }

    /*** get delete ***/
    public function getDelete($serviceID)
    {
      if(is_null($service = $this->language->find($serviceID))){
        return Redirect::to('admin/services')->with('errors','Dienesta ieraksts netika atrasts');
      }
      $service->delete();

      return Redirect::to('admin/services')->with('success','Dienesta ieraksts veiksmīgi izdzēsts');
    }

    /*** SERVICE JSON LIST ***/
    public function getServiceListJSON()
    {
      $serviceList = array();
      $services = $this->service->all();
      if(count($services)>0){
        foreach($services as $service){
          $serviceList["items"][] = array(
            'name' => html_entity_decode($service->name,0,'UTF-8'),
            'address' => html_entity_decode($service->address,0,'UTF-8'),
            'phone' => $service->phone,
            'mail' => $service->mail,
            'description' => html_entity_decode($service->description,0,'UTF-8'),
            'latlng' => $service->latlng
          );
        }
      }else{
        $serviceList['items'] = array();
      }
      return Response::json($serviceList);
    }

}