<?php namespace Controllers\Admin;

use AdminController;
use Config;
use Input;
use Lang;
use URL;
use Redirect;
use Sentry;
use Validator;
use View;
use Response;
use Taxi;
use Geocoder;

class TaxiController extends AdminController {

	protected $taxi;

	public function __construct(Taxi $taxi)
	{
		$this->taxi = $taxi;
	}

	/*** return main view ***/
	public function getindex()
	{
		//get all taxi records
		$taxiList = $this->taxi->orderBy('name','ASC')->get();
		return View::make('backend/taxi/index',compact('taxiList'));
	}

	/*** create new record ***/
	public function postcreate()
	{
		//get data and save it
		$taxiInfo = new Taxi;
		if($taxiInfo->validate(Input::all())){
			$taxiInfo->name = e(Input::get('name'));
			$taxiInfo->phone = e(Input::get('phone'));
			$taxiInfo->address = e(Input::get('address'));
			//try to geocode
			try {
				$taxiGeocode = Geocoder::geocode(e(Input::get('address')));
				$taxiInfo->latlng = $taxiGeocode->getLatitude().",".$taxiGeocode->getLongitude();
			}catch(Exception $e){
				$taxiInfo->latlng = null;
			}
			if($taxiInfo->save()){
				return Redirect::to('admin/taxi')->with('success','Taksometra ieraksts veiksmīgi pievienots');
			}
		}else{
			return Redirect::back()->withInput()->withErrors($taxiInfo->errors());
		}
		return Redirect::to('admin/taxi')->with('error','Nevarēja izveidot taksometra ierakstu');
	}

	/** return records edit view with data ***/
	public function getEdit($id)
	{
		//get taxi data
		if(is_null($taxi = $this->taxi->find($id))){
			return Redirect::to('admin/taxi')->with('error','Nevarēja atrast taksometra ierakstu');
		}
		return View::make('backend/taxi/edit',compact('taxi'));
	}

	/*** edit record and redirect ***/
	public function postEdit($id)
	{
		//get taxi data and save if can
		if(is_null($taxi = $this->taxi->find($id))){
			return Redirect::to('admin/taxi')->with('error','Nevarēja atrast taksometra ierakstu');
		}
		if($taxi->validate(Input::all())){
			$taxi->name = e(Input::get('name'));
			$taxi->address = e(Input::get('address'));
			$taxi->phone = e(Input::get('phone'));
			try {
				$taxiGeocode = Geocoder::geocode(e(Input::get('address')));
				$taxi->latlng = $taxiGeocode->getLatitude().",".$taxiGeocode->getLongitude();
			}catch (Exception $e){}
			if($taxi->save()){
				return Redirect::to('admin/taxi')->with('success','Taksometra ieraksts veiksmīgi atjaunināts');
			}
		}else{
			return Redirect::back()->withInput()->withErrors($taxi->errors());
		}
		return Redirect::to('admin/taxi')->with('error','Nevarēja atrasts taksometra ierakstu');
	}

	/*** get modal for delete ***/
	public function getModalDelete($id)
	{
		//set data and show modal
		$model = 'taxi';
		$confirm_route = $error = null;
		if(is_null($taxi = $this->taxi->find($id))){
			$error = 'Nevarēja atrast izvēlēto taksometra ierakstu';
			return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
		}
		$confirm_route = URL::action('delete/taxi',array('id'=>$taxi->id));
		return View::make('backend/layouts/modal_confirmation',compact('error','model','confirm_route'));
	}

	/*** delete record ***/
	public function getDelete($id)
	{
		//validate data and remove record
		if(is_null($taxi = $this->taxi->find($id))){
			return Redirect::to('admin/taxi')->with('error','Nevarēja atrast taksometra ierakstu');
		}
		$taxi->delete();
		return Redirect::to('admin/taxi')->with('success','Taksometra ieraksts veiksmīgi izdzēsts');
	}

	/*** get json response ***/
	public function getJsonList()
	{
		//get all taxi and return in json
		$taxiListArray = array();
		$taxiList = $this->taxi->orderBy('name','ASC')->get();
		if(count($taxiList)>0){
			foreach($taxiList as $taxi){
				$taxiListArray[] = array(
					'name' => html_entity_decode($taxi->name, 0, 'UTF-8'),
					'address' => html_entity_decode($taxi->address,0,'UTF-8'),
					'phone' => $taxi->phone,
					'latlng' => $taxi->latlng
				);
			}
		}
		return Response::json(array('Items'=>$taxiListArray));
	}

}