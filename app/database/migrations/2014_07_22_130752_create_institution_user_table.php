<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitutionUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('institution_users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('institution_id');
			$table->string('name',150);
			$table->string('job',255);
			$table->string('mail',255)->nullable();
			$table->string('phone',100)->nullable();
			$table->integer('weight');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('institution_users');
	}

}
