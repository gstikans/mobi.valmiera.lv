<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAudioguidesTable extends Migration {

	public function up()
	{
		Schema::create('audioguides', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('places_id')->unsigned()->nullable();
			$table->string('file_lv', 1000);
			$table->string('file_en', 1000)->nullable();
			$table->string('file_ru', 1000)->nullable();
			$table->string('file_est', 1000)->nullable();
			$table->integer('order')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('audioguides');
	}
}