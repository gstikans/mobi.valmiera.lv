<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGalleryImagesTable extends Migration {

	public function up()
	{
		Schema::create('gallery_images', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('gallery_id')->unsigned();
			$table->string('file', 1000);
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('gallery_images');
	}
}