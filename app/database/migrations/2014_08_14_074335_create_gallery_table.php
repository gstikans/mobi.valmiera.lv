<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateGalleryTable extends Migration {

	public function up()
	{
		Schema::create('gallery', function(Blueprint $table) {
			$table->increments('id');
			$table->integer('places_id')->unsigned()->nullable();
			$table->string('name_lv', 255);
			$table->string('name_en', 255)->nullable();
			$table->string('name_ru', 255)->nullable();
			$table->string('name_est', 255)->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('gallery');
	}
}