<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePlacesTable extends Migration {

	public function up()
	{
		Schema::create('places', function(Blueprint $table) {
			$table->increments('id');
			$table->string('name_lv', 255);
			$table->string('name_en', 255)->nullable();
			$table->string('name_ru', 255)->nullable();
			$table->string('name_est', 255)->nullable();
			$table->text('about_lv')->nullable();
			$table->text('about_en')->nullable();
			$table->text('about_ru')->nullable();
			$table->text('about_est')->nullable();
			$table->string('latlng', 255);
			$table->integer('order')->nullable();
			$table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('places');
	}
}