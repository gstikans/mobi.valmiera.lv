<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateForeignKeys extends Migration {

	public function up()
	{
		Schema::table('audioguides', function(Blueprint $table) {
			$table->foreign('places_id')->references('id')->on('places')
						->onDelete('set null')
						->onUpdate('set null');
		});
		Schema::table('gallery', function(Blueprint $table) {
			$table->foreign('places_id')->references('id')->on('places')
						->onDelete('set null')
						->onUpdate('set null');
		});
		Schema::table('gallery_images', function(Blueprint $table) {
			$table->foreign('gallery_id')->references('id')->on('places')
						->onDelete('cascade')
						->onUpdate('cascade');
		});
	}

	public function down()
	{
		Schema::table('audioguides', function(Blueprint $table) {
			$table->dropForeign('audioguides_places_id_foreign');
		});
		Schema::table('gallery', function(Blueprint $table) {
			$table->dropForeign('gallery_places_id_foreign');
		});
		Schema::table('gallery_images', function(Blueprint $table) {
			$table->dropForeign('gallery_images_gallery_id_foreign');
		});
	}
}