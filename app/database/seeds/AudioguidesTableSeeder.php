<?php


class AudioguidesTableSeeder extends Seeder {

	public function run()
	{
		$fileCount = 45;
		$uploadDir = DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."audioguides".DIRECTORY_SEPARATOR;
		//public_path().$this->uploadDir.$language.DIRECTORY_SEPARATOR,$savedFileName
		DB::table('audioguides')->delete();

		for($i=1;$i<$fileCount;$i++){
			$placeInfo = DB::table('places')->where('order',$i)->first();
			$fileName = $i.".mp3";
			$filePathLV = public_path().$uploadDir."lv".DIRECTORY_SEPARATOR.$fileName;
			$filePathEN = public_path().$uploadDir."en".DIRECTORY_SEPARATOR.$fileName;
			$filePathRU = public_path().$uploadDir."ru".DIRECTORY_SEPARATOR.$fileName;
			$filePathEST = public_path().$uploadDir."est".DIRECTORY_SEPARATOR.$fileName;
			DB::table('audioguides')->insert([
				'places_id' => $placeInfo->id,
				'file_lv' => $filePathLV,
				'file_en' => $filePathEN,
				'file_ru' => $filePathRU,
				'file_est' => $filePathEST,
				'order' => $i
			]);
		}
	}

}