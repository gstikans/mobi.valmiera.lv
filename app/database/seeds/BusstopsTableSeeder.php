<?php

class BusstopsTableSeeder extends Seeder {

	public function run()
	{
		DB::table("busstops")->delete();
		$busStopList = $this->getBusStops();
		$busCoordinates = $this->getBusCoordinates();
		//make combined array
		$busCombined = array_merge_recursive($busStopList,$busCoordinates);
		if(count($busCombined) > 0){
			foreach($busCombined as $name=>$params){
				//download files
				$fileName = $this->getBusStopFile($params["link"]);
				//insert file
				DB::table('busstops')->insert(
	                array('name'=>e($name),'file'=>$fileName,'latlng'=>$params["latlng"])
	            );
			}
		}
	}

	function getBusStopFile($filePath)
	{
		//get save filename
		if($filePath){
			$filePathChunks = explode("/",str_replace("http://","",$filePath));
			$oldName = $filePathChunks[count($filePathChunks)-1];
			$fileName = str_replace(",","_",str_replace(" ","_",$filePathChunks[count($filePathChunks)-1]));
			unset($filePathChunks[count($filePathChunks)-1]);
			$cleanUrl = "http://".implode("/",$filePathChunks)."/".rawurlencode($oldName);
			//upload path
			$uploadPath = public_path().DIRECTORY_SEPARATOR."uploads".DIRECTORY_SEPARATOR."busstops".DIRECTORY_SEPARATOR;
			//set download options
			$options = array(
				//CURLOPT_FILE => $uploadPath.$fileName,
				CURLOPT_TIMEOUT => 28800,
				CURLOPT_URL => $cleanUrl,
				CURLOPT_RETURNTRANSFER => 1,
				CURLOPT_REFERER => 'http://valmiera.lv',
				CURLOPT_HEADER => 0
			);
			$curlInit = curl_init();
			curl_setopt_array($curlInit, $options);
			$curlResult = curl_exec($curlInit);
			//write to file
			$fileSave = fopen($uploadPath.$fileName,'w');
			fwrite($fileSave,$curlResult);
			fclose($fileSave);
			//try to convert file if possible
			if(file_exists($uploadPath.$fileName) && class_exists("Imagick")){
				$fileNameConverted = substr($fileName,0,-4).".jpg";
				$image = new \Imagick();
				$image->setResolution(100,100);
				$image->readImage($uploadPath.$fileName);
				$image->setImageFormat('jpeg');
				$image->setImageCompression(\Imagick::COMPRESSION_JPEG);
				$image->setImageCompressionQuality(90);
				$image->writeImage($uploadPath.$fileNameConverted);
				$image->clear();
				$image->destroy();
				//delete old file
				unlink($uploadPath.$fileName);
				return "/uploads/busstops/".$fileNameConverted;
			}else{
				return "/uploads/busstops/".$fileName;
			}
		}
		return null;
	}

	function getBusStops()
	{
		$busList = array();
        $baseUri = "http://www.vtu-valmiera.lv";
        $pathUri = "/index.php/pasazieru-parvadajumi/pilsetas-satiksme/saraksti";
        //get all page html info
        $busHTML = file_get_contents($baseUri.$pathUri);
        //try to create dom element
        libxml_use_internal_errors(true);
        $busDom = new \DOMDocument;
        $busDom->loadHTML($busHTML);
        libxml_use_internal_errors(false);
        $busXPath = new \DOMXPath($busDom);
        //get bus elements
        $busElements = $busXPath->evaluate('//table[@class="stopstable"]/tbody/tr/td/a');
        if($busElements->length > 0){
            foreach($busElements as $busElement){
                $busList[trim($busElement->textContent)]["link"] = $baseUri.$busElement->getAttribute('href');
            }
        }
        return $busList;
	}

	function getBusCoordinates()
	{
		$markerList = array();
		$markerUri = "https://mapsengine.google.com/map/kml?hl=lv&authuser=0&mid=z-UA-IVF2VsY.kfE46UN9SsCY&lid=z-UA-IVF2VsY.kCX9ETie1CQo&cid=mp&cv=u8ZF_LbUMoA.lv.";
		$markerXML = file_get_contents($markerUri);
		$parsedXML = simplexml_load_string($markerXML);
		//iterate markers
		foreach($parsedXML->Document->Placemark as $placeMark){
			list($lng,$lat,$alt) = explode(",",$placeMark->Point->coordinates);
			$markerList["".$placeMark->name]["latlng"] = $lat.",".$lng;
		}
		return $markerList;
	}

}