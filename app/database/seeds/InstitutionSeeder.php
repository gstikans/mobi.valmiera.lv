<?php

class InstitutionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table("institution_users")->delete();
        DB::table("institutions")->delete();
        //get institution list and organize it
        $institutions = file_get_contents('http://valmiera.lv/kontaktinodalas_json');
        $institutions = json_decode($institutions);
        $instituionList = array();
        $i=1;
        if(count($institutions->Items)>0){
            foreach($institutions->Items as $institution){
                if(!array_key_exists($institution->node->name, $instituionList)){
                    $instituionList[$institution->node->name] = $i;
                    $i++;
                }
            }
        }
        //insert institution records
        foreach($instituionList as $name=>$weight){
            DB::table('institutions')->insert(
                array('name'=>$name,'weight'=>$weight)
            );
        }

        //get full user list
        $users = file_get_contents('http://valmiera.lv/kontakti_json');
        $users = json_decode($users);
        if(count($users->Items)>0){
            $i=1;
            foreach($users->Items as $user){
                //find right institution by its name
                $institutionInfo = DB::table('institutions')->where('name',$user->node->name)->first();
                if($institutionInfo !== null){
                    //values
                    $name = $user->node->title;
                    $job = $user->node->job;
                    $mail = (isset($user->node->pasts)) ? $user->node->pasts : '';
                    $phone = (isset($user->node->phone)) ? $user->node->phone : '';
                    //if institution exists insert user
                    DB::table('institution_users')->insert(
                        array('institution_id'=>$institutionInfo->id, 'name'=>$name, 'job'=>$job, 'mail'=>$mail, 'phone'=>$phone, 'weight'=>$i)
                    );
                $i++;}
            }
        }

    }
}
