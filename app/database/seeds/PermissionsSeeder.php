<?php

class PermissionsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('groups')->delete();
        //get default user undeletable
        $user = Sentry::findUserById(1);
        //create a super user group
        $group = Sentry::getGroupProvider()->create(array(
            'name'        => 'Admin',
            'permissions' => array(
                'superuser'    => 1,
                'admin' => 1
            )
        ));
        //add user to superuser group
        $user->addGroup($group);

        //create module permissions [languages,places,events,services,municipality,busstops,taxi,audio guide,gallery,where to eat,hotel]
        $group = Sentry::getGroupProvider()->create(array(
            'name' => 'Places manager',
            'permissions' => array(
                'places.read' => 1,
                'places.create' => 1
            )
        ));
        //events
        $group = Sentry::getGroupProvider()->create(array(
            'name' => 'Event manager',
            'permissions' => array(
                'events.read' => 1,
                'events.create' => 1
            )
        ));
        //services
        $group = Sentry::getGroupProvider()->create(array(
            'name' => 'Service manager',
            'permissions' => array(
                'services.read' => 1,
                'services.create' => 1
            )
        ));
        //municipality
        $group = Sentry::getGroupProvider()->create(array(
            'name' => 'Municipality manager',
            'permissions' => array(
                'municipality.read' => 1,
                'municipality.create' => 1
            )
        ));
        //busstops
        $group = Sentry::getGroupProvider()->create(array(
            'name' => 'Bus stop manager',
            'permissions' => array(
                'busstops.read' => 1,
                'busstops.create' => 1
            )
        ));
        //taxi
        $group = Sentry::getGroupProvider()->create(array(
            'name' => 'Taxi manager',
            'permissions' => array(
                'taxi.read' => 1,
                'taxi.create' => 1
            )
        ));
        //audioguide
        $group = Sentry::getGroupProvider()->create(array(
            'name' => 'Audio guide manager',
            'permissions' => array(
                'audioguide.read' => 1,
                'audioguide.create' => 1
            )
        ));
        //gallery
        $group = Sentry::getGroupProvider()->create(array(
            'name' => 'Gallery manager',
            'permissions' => array(
                'gallery.read' => 1,
                'gallery.create' => 1
            )
        ));
        //where to eat
        $group = Sentry::getGroupProvider()->create(array(
            'name' => 'Restaurant manager',
            'permissions' => array(
                'restaurants.read' => 1,
                'restaurants.create' => 1
            )
        ));
        //hotel
        $group = Sentry::getGroupProvider()->create(array(
            'name' => 'Hotel manager',
            'permissions' => array(
                'hotels.read' => 1,
                'hotels.create' => 1
            )
        ));
    }
}
