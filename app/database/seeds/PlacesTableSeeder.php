<?php

use Stichoza\Google\GoogleTranslate;

class PlacesTableSeeder extends Seeder {

	public function run()
	{
		$markerList = $this->getCoordinates();
		if(count($markerList) > 0){
			DB::table('places')->delete();
			foreach($markerList as $name=>$params){
				$nameSplit = explode('.',$name);
				$order = $nameSplit[0];
				unset($nameSplit[0]);
				$name = implode(".", $nameSplit);
				DB::table('places')->insert(
					[
						'name_lv'=>e($name),
						'name_en'=> $this->getNameInLanguage('en_names',$order),//GoogleTranslate::staticTranslate($name, "lv", "en"),
						'name_ru'=> $this->getNameInLanguage('ru_names',$order),//GoogleTranslate::staticTranslate($name, "lv", "ru"),
						'name_est'=> $this->getNameInLanguage('est_names',$order),//GoogleTranslate::staticTranslate($name, "lv", "et"),
						'about_lv'=>'',
						'about_en'=>'',
						'about_ru'=>'',
						'about_est'=>'',
						'latlng'=>$params['latlng'],
						'order'=> $order
					]
				);
			}
		}
	}

	function getCoordinates()
	{
		$markerList = array();
		$markerUri = "https://www.google.com/maps/d/kml?hl=lv&authuser=0&mid=z-UA-IVF2VsY.koUHGrijWnL8&lid=z-UA-IVF2VsY.klYLNTnY8h-I&cid=mp&cv=zbHY06hPI94.lv.";
		$markerXML = file_get_contents($markerUri);
		$parsedXML = simplexml_load_string($markerXML);
		//iterate markers
		foreach($parsedXML->Document->Placemark as $placeMark){
			list($lng,$lat,$alt) = explode(",",$placeMark->Point->coordinates);
			$markerList["".$placeMark->name]["latlng"] = $lat.",".$lng;
		}
		return $markerList;
	}

	//language names
	function getNameInLanguage($lng,$id){
		$en_names = 
			[	'St. Simon’s Church',
				'Town Square',
				'Livonian Castle Ruins',
				'Beverina County Arts and Crafts Shop',
				'Old Pharmacy',
				'Valmiera Museum',
				'Marshner’s House',
				'The statue of “The Reclining Amazon"',
				'Walters Hill',
				'The Music School',
				'Dwelling House',
				'Laundry Drying Premises',
				'Luca Hill',
				'The Memorial',
				'Island of Idols',
				'Jānis Daliņš Stadium',
				'The Steep Banks of the Gauja and the Park of Senses',
				'Goat Rapids',
				'Riga Gate',
				'Valmiera Drama Theatre',
				'The Millpond',
				'Gallows Hill',
				'Valmiera Parsonage',
				'The Blue Bastion',
				'The Hanse Wall',
				'Sculpture: The Fountain of Light',
				'Valmiera Culture Centre',
				'Bachelors Park',
				'Cinema House „Gaisma”',
				'Orthodox Church of St. Sergey of Radonezh',
				'Ģīme Watermill',
				'Valmiera State Gymnasium',
				'Narrow-Gauge Railway Bridge',
				'Sculpture: Gift to the New Millennium',
				'Vidzeme Olympic Centre',
				'The Gauja Bridge',
				'Vidzeme University of Applied Sciences',
				'Sculpture The Boys of Valmiera',
				'Firefighters’ Tower',
				'Pārgauja Craftsmen’s Maisonette',
				'Catholic Church',
				'The Art Gallery ”Laipa”',
				'Valmiermuiža Beer Brewery',
				'Active Recreation Complex "Avoti" and BMX Museum'
			];
		$ru_names =
			[
				'Собор Святого Симеона',
				'Ратушная площадь',
				'Развалины замка Ливонского ордена',
				'Лавка ремесленников Беверинского края',
				'Старая аптека',
				'Валмиерский музей',
				'Дом Маршнера',
				'Скульптура « Лежащая амазонка»',
				'Горка Валтеркалниньш',
				'Музыкальная школа',
				'Жилой дом',
				'Сушильня белья',
				'Гора Луцас',
				'Мемориальный ансамбль',
				'Островок Элку',
				'Стадион Яниса Далиньша',
				'Крутые берега Гауи и Парк ощущений',
				'Козьи пороги',
				'Рижские ворота', 
				'Валмиерский драматический театр',
				'Мельничное озерцо',
				'Виселичная горка',
				'Валмиерское пасторское поместье',
				'Синий бастион',
				'Ганзейская стена',
				'Скульптура «Фонтан света»',
				'Валмиерский Центр культуры',
				'Парк холостяков',
				'Кинотеатр Gaisma',
				'Собор святого Сергия Радонежского',
				'Гимская мельница',
				'Валмиерская Государственная гимназия',
				'Мост узкоколейной железной дороги',
				'Скульптура «Подарок грядущему тысячелетию»',
				'Видземский Олимпийский центр',
				'Мост через Гаую',
				'Видземская Высшая школа',
				'Скульптура «Валмиерские ребята»',
				'Пожарная башенка',
				'Паргауйский Домик ремесленников',
				'Католическая церковь',
				'Художественная галерея Galerija Laipa',
				'Валмиермуйжская пивоварня',
				'Комплекс активного отдыха Avoti и музей BMX'
			];
		$est_names = 
			[
				'Püha Siimona kirik',
				'Raekoja plats',
				'Liivimaa ordulossi varemed',
				'Beveriina käsitööliste kauplus',
				'Vana apteek',
				'Valmiera muuseum',
				'Marschneri maja',
				'Skulptuur „Lamav amatsoon”',
				'Valteri küngas',
				'Muusikakool',
				'Elumaja Pilskalna (Linnuse) tänav 2',
				'Pesukuivati Pilskalna tänav 2',
				'Lucas mägi',
				'Memoriaalne ansambel',
				'Puuslike saareke',
				'Jānis Daliņši staadion',
				'Koiva pank ja Tunnetuste park',
				'Kitsede kosk',
				'Riia värav',
				'Valmiera draamateater',
				'Veskijärv',
				'Võllaküngas', 
				'Valmiera õpetajamõis',
				'Sinine bastion',
				'Hansa sein',
				'Skulptuur „Valguse purskkaev”',
				'Valmiera Kultuurikeskus',
				'Vanapoiste park',
				'Kino „Gaisma',
				'Püha Sergi Radonežski kirik',
				'Ģīme veski',
				'Valmiera Riigigümnaasium',
				'Kitsarööpmelise raudtee sild',
				'Skulptuur „Kingitus järgnevale milleeniumile”',
				'Vidzeme olümpiakeskus',
				'Koiva sild',
				'Vidzeme Kõrgkool',
				'Skulptuur „Valmiera poisid”',
				'Tuletõrje tornike',
				'Üle-Gauja Käsitööliste maja',
				'Katoliku kirik',
				'Kunstigalerii „Laipa”',
				'Valmiermõisa õllepruul',
				'Aktiivse puhkuse kompleks „Avoti” ja BMX muuseum'
			];
		return ${$lng}[($id-1)];
	}

}