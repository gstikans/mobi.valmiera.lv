<?php

/**
* Language file for language delete modal
*
*/
return array(

    'body'			=> 'Vai esat drošs ka vēlaties dzēst šo ierakstu. Šī darbība ir neatgriezeniska',
    'cancel'		=> 'Aizvērt',
    'confirm'		=> 'Dzēst',
    'title'         => 'Dzēst ierakstu',

);
