<?php
/**
* Language file for group error/success messages
*
*/

return array(

    'group_exists'        => 'Šāda grupa eksistē!',
    'group_not_found'     => 'Grupa ar ID [:id] neeksistē.',
    'group_name_required' => 'Lauks nosaukums ir obligāts',

    'success' => array(
        'create' => 'Grupa izveidota veiksmīgi.',
        'update' => 'Grupa atjaunināta veiksmīgi.',
        'delete' => 'Grupa dzēsta veiksmīgi.',
    ),

    'delete' => array(
        'create' => 'There was an issue creating the group. Please try again.',
        'update' => 'There was an issue updating the group. Please try again.',
        'delete' => 'There was an issue deleting the group. Please try again.',
    ),

);
