<?php

/**
* Language file for group delete modal
*
*/
return array(

    'title'         => 'Dzēst grupu',
    'body'			=> 'Vai esat pārliecināts, ka vēlaties dzēst grupu. Šī darbība ir neatgriezeniska.',
    'cancel'		=> 'Atcelt',
    'confirm'		=> 'Dzēst',

);
