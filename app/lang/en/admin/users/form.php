<?php
/**
* Language file for form fields for user account management
*
*/

return array(

    'activated'				=> 'Aktīvs',
    'changegravatar'		=> 'Change your avatar at Gravatar.com',
    'confirmemail'			=> 'Apstiprini e-pastu',
    'confirmpassword'		=> 'Paroles apstiprināšana',
    'country'				=> 'Valsts',
    'email'					=> 'E-pasts',
    'firstname'				=> 'Vārds',
    'gravataremail'			=> 'Gravatar Email',
    'grouphelp'				=> 'Izvēlies lietotāja tiesību grupu.',
    'groups'				=> 'Grupa',
    'lastname'				=> 'Uzvārds',
    'newemail'				=> 'Jauns e-pasts',
    'newpassword'			=> 'Jauna parole',
    'oldpassword'			=> 'Esošā parole',
    'password'				=> 'Parole',
    'website'				=> 'Website',

);
