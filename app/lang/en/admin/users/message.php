<?php
/**
* Language file for user error/success messages
*
*/

return array(

    'user_exists'              => 'Lietotājs jau eksistē!',
    'user_not_found'           => 'Lietotājs ar ID [:id] neeksistē.',
    'user_login_required'      => 'Pieslēgšānās lauks obligāts',
    'user_password_required'   => 'Obligāti jāievada parole.',
    'insufficient_permissions' => 'Nepietiekamas tiesības.',
    'banned'              => 'bloķēts',
    'suspended'             => 'bloķēts',

    'success' => array(
        'create'    => 'Lietotājs veiksmīgi izveidots.',
        'update'    => 'Lietotājs veiksmīgi atjaunināts.',
        'delete'    => 'Lietotājs veiksmīgi dzēsts.',
        'ban'       => 'User was successfully banned.',
        'unban'     => 'User was successfully unbanned.',
        'suspend'   => 'User was successfully suspended.',
        'unsuspend' => 'User was successfully unsuspended.',
        'restored'  => 'User was successfully restored.'
    ),

    'error' => array(
        'create' => 'Radās kļūme veidojot lietotāja kontu, lūdzu mēģiniet vēlreiz.',
        'update' => 'Radās kļūme atjauninot lieototāja kontu, lūdzu mēģiniet vēlreiz.',
        'delete' => 'Radās kļūme dzēšot lietotāja kontu, lūdzu mēģiniet vēlreiz.',
    ),

);
