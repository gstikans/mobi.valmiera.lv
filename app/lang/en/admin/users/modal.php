<?php

/**
* Language file for user delete modal
*
*/
return array(

    'body'			=> 'Vai esat pārliecināts, ka vēlaties dzēst lietotāja kontu. Šī darbība nav atceļama',
    'cancel'		=> 'Atcelt',
    'confirm'		=> 'Dzēst',
    'title'         => 'Dzēst lietotāja kontu',

);
