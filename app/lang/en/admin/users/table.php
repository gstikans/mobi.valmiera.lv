<?php
/**
* Language file for user table headings
*
*/
return array(

    'activated'  => 'Aktīvs',
    'created_at' => 'Izveidots',
    'email'      => 'E-pasts',
    'first_name' => 'Vārds',
    'id'         => 'Id',
    'last_name'  => 'Uzvārds',

);
