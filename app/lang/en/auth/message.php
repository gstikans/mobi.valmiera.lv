<?php
/**
* Language file for auth error messages
*
*/

return array(

    'account_already_exists' => 'Lietotājs ar šādu e-pastu jau eksistē.',
    'account_not_found'      => 'Lietotāja vārds un/vai parole nav pareizs.',
    'account_not_activated'  => 'Šis lietotāja konts nav aktivizēts.',
    'account_suspended'      => 'Lietotāja konts bloķēts. Pārāk daudz neveiksmīgu mēģinājumu.',
    'account_banned'         => 'Lietotāja konts bloķēts.',

    'signin' => array(
        'error'   => 'Radās kļūme piesakoties sistēmā.',
        'success' => 'Veiksmīgi ielogojies.',
    ),

    'signup' => array(
        'error'   => 'There was a problem while trying to create your account, please try again.',
        'success' => 'Account sucessfully created.',
    ),

        'forgot-password' => array(
            'error'   => 'There was a problem while trying to get a reset password code, please try again.',
            'success' => 'Password recovery email successfully sent.',
        ),

        'forgot-password-confirm' => array(
            'error'   => 'There was a problem while trying to reset your password, please try again.',
            'success' => 'Your password has been successfully reset.',
        ),

    'activate' => array(
        'error'   => 'There was a problem while trying to activate your account, please try again.',
        'success' => 'Your account has been successfully activated.',
    ),

);
