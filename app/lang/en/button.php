<?php
/**
* Language file for submit buttons across the site
*
*/
return array(

    'edit'    			=> 'Labot',
    'delete'  			=> 'Dzēst',
    'restore' 			=> 'Atjaunot',
    'publish' 			=> 'Publicēt',
    'save'	  			=> 'Saglabāt',
    'submit'	  		=> 'Iesniegt',
    'cancel'  			=> 'Atcelt',
    'create'  			=> 'Izveidot',
    'back'    			=> 'Atpakaļ',
    'signin'  			=> 'Ielogoties',
    'forgotpassword' 	=> 'I forgot my password',
    'rememberme'		=> 'Remember me',
    'signup'			=> 'Sign-Up',
    'update'			=> 'Atjaunināt',

);
