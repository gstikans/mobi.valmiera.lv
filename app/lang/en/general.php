<?php
/**
* Language file for general strings
*
*/
return array(

    'no'  			=> 'Nē',
    'noresults'  	=> 'Nav rezultātu',
    'site_name'		=> 'Mobi.Valmiera',
    'yes' 			=> 'Jā',

);
