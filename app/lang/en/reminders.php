<?php
/**
* Language file reminders
*
*/
return array(

    /*
    |--------------------------------------------------------------------------
    | Password Reminder Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    "password" 		=> "Parolei jāsatur vismaz 6 simbolus un jasakrīt ar apstiprināto.",
    "user" 			=> "Nederīgs lietotājs un/vai parole.",
    "token" 		=> "Paroles atjaunošanas atslēga nederīga.",
    "sent" 			=> "Paroles atgādinājums izsūtīts!",

);
