<?php

class Audioguide extends Elegant {

	protected $table = 'audioguides';
	public $timestamps = true;

	protected $rules = [
		'file-lv' => 'required|max:15360|mimes:mp3,mpga',
		'file-en' => 'max:15360|mimes:mp3,mpga',
		'file-ru' => 'max:15360|mimes:mp3,mpga',
		'file-est' => 'max:15360|mimes:mp3,mpga',
	];

	protected $messages = array(
		'file-lv.required' => 'Nepieciešams ielādēt failu vismaz latviešu valodā',
		'file-lv.mimes' => 'Atļauts ielādēt tikai MP3 failus',
		'file-lv.max' => 'Fails nedrīkst būt lielāks par 15MB',
		'file-en.mimes' => 'Atļauts ielādēt tikai MP3 failus',
		'file-en.max' => 'Fails nedrīkst būt lielāks par 15MB',
		'file-ru.mimes' => 'Atļauts ielādēt tikai MP3 failus',
		'file-ru.max' => 'Fails nedrīkst būt lielāks par 15MB',
		'file-est.mimes' => 'Atļauts ielādēt tikai MP3 failus',
		'file-est.max' => 'Fails nedrīkst būt lielāks par 15MB',
	);

	public function fileName($language="lv")
	{
		$nameChunks = explode(DIRECTORY_SEPARATOR,$this->{"file_".$language});
		return $nameChunks[count($nameChunks)-1];
	}

	public function audioPlace()
	{
		return $this->belongsTo('Place','places_id');
	}

	public function setValidateRules($rules = [])
	{
		if(!empty($rules)){
			$this->rules = $rules;
		}
	}

	public function delete()
	{
		//remove file if it exists
		$this->fileDelete("lv");
		$this->fileDelete('en');
		$this->fileDelete('ru');
		$this->fileDelete('est');
		return parent::delete();
	}

	public function fileDelete($language = "lv")
	{
		if($this->{'file_'.$language} != "" && file_exists(public_path().$this->{'file_'.$language})){
			unlink(public_path().$this->{'file_'.$language});
		}
		$this->{'file_'.$language} = "";
		$this->save();
	}

}