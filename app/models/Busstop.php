<?php

class Busstop extends Elegant {
	protected $fillable = [];

	protected $rules = array(
		'name' => 'required|min:3',
		'latlng' => 'required',
	);

	protected $messages = array(
		'name.required' => 'Lauks nosaukums ir obligāts',
		'name.min' => 'Nosaukuma minimālais garums ir :min simboli',
		'name.unique' => 'Autobusu pietura ar šādu nosaukumu jau eksistē',
		'latlng.required' => 'Kārtē ir jābūt norādītai autobusu pierurai',
	);

	public function fileName()
	{
		$nameChunks = explode(DIRECTORY_SEPARATOR,$this->file);
		return $nameChunks[count($nameChunks)-1];
	}

	public function delete()
	{
		//remove file if it exists
		$this->fileDelete();
		return parent::delete();
	}

	public function fileDelete()
	{
		if($this->file !== "" && file_exists(public_path().$this->file)){
			unlink(public_path().$this->file);
		}
		$this->file = "";
		$this->save();
	}

}