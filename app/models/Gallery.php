<?php

class Gallery extends Elegant {

	protected $table = 'gallery';
	public $timestamps = true;

	public function galleryImages()
	{
		return $this->hasMany('GalleryImage', 'gallery_id');
	}

}