<?php

class Hotel extends Elegant {
	protected $fillable = [];

	protected $rules = array(
		'name' => 'required|min:3',
		'address' => 'required|min:3',
		'mail' => 'email',
		'web' => 'url'
	);

	protected $messages = array(
		'name.required' => 'Lauks nosaukums ir obligāts',
		'name.min' => 'Nosaukuma minimālais garums ir :min simboli',
		'name.unique' => 'Vieta ar šādu nosaukumu jau eksistē',
		'address.required' => 'Lauks adrese ir obligāts',
		'address.min' => 'Minimālais adreses garums ir :min simboli',
		'mail.email' => 'E-pasta laukam ir jāsatur derīga e-pasta adrese',
		'web.url' => 'Mājaslapas laukam jāsatur derīga adrese piem.: http://www.adrese.lv',
	);
	
}