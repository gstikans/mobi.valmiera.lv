<?php

class Institution extends Elegant
{
    protected $rules = array(
            'name'   => 'required|min:3|unique:institutions'
    );

    public function delete()
    {
        $this->institutionUsers()->delete();
        // Delete institution
        return parent::delete();
    }

    public function name()
    {
        return $this->name;
    }

    public function weight()
    {
        return $this->weight;
    }

    //Users
    public function institutionUsers()
    {
        return $this->hasMany('InstitutionUser')->orderBy('weight','ASC');
    }

}
