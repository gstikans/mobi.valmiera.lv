<?php

class InstitutionUser extends Elegant
{
    protected $rules = array(
            'name'   => 'required|min:3|unique:institution_users',
            'institution_id' => 'required'
    );

    protected $messages = array(
        'name.required' => 'Nepieciešams ievadīt vārdu un uzvārdu',
        'name.min' => 'Minimālais vārda, uzvārda garums ir :min simboli',
        'name.unique' => 'Persona ar šādu vārdu jau eksistē',
        'institution_id.required' => 'Nevarēja noskaidrot kurā struktūrvienībā',
    );

    public function delete()
    {
        // Delete institution user
        return parent::delete();
    }

    public function name()
    {
        return html_entity_decode($this->name,0,'UTF-8');
    }

    public function job()
    {
        return html_entity_decode($this->job,0,'UTF-8');
    }

    public function phone()
    {
        return str_replace(" ","",$this->phone);
    }

    public function mail()
    {
        return $this->mail;
    }

    public function weight()
    {
        return $this->weight;
    }

    //Users
    public function institution()
    {
        return $this->belongsTo('Institution');
    }

}
