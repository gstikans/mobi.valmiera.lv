<?php

class Place extends Elegant {

	protected $table = 'places';
	public $timestamps = true;

	protected $rules = array(
		'name_lv' => 'required|min:3',
		'name_en' => 'min:3',
		'name_ru' => 'min:3',
		'name_est' => 'min:3',
		'latlng' => 'required'
	);

	protected $messages = array(
		'name_lv.required' => 'Nepieciešams ievadīt nosaukumu vismaz latviešu valodā',
		'name_lv.min' => 'Minimālais nosaukuma garums ir :min simboli',
		'name_en.min' => 'Minimālais nosaukuma garums angļu valodā ir :min simboli',
		'name_ru.min' => 'Minimālais nosaukuma garums krievu valodā ir :min simboli',
		'name_est.min' => 'Minimālais nosaukuma garums igauņu valodā ir :min simboli',
		'latlng.required' => 'Nepieciešams norādīt vietu kartē'
	);

	public $languageList = array('lv','en','ru','est');

	public function placeAudio()
	{
		return $this->hasOne('Audioguide', 'places_id');
	}

	public function placeGallery()
	{
		return $this->hasOne('Gallery', 'places_id');
	}

}