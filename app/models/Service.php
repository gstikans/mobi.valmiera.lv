<?php

class Service extends Elegant
{
    protected $rules = array(
            'name'   => 'required|min:3|unique:services',
            'address'   => "required|min:3",
            'phone' => 'min:8',
    );

    public function delete()
    {
        // Delete service
        return parent::delete();
    }

    public function name()
    {
        return $this->name;
    }

    public function address()
    {
        return $this->address;
    }

    public function phone()
    {
        return $this->phone;
    }

    public function mail()
    {
        return $this->mail;
    }

    public function description()
    {
        return $this->description;
    }

    public function latlng()
    {
        return $this->latlng;
    }

}
