<?php

class Taxi extends Elegant {

	protected $table = "taxi";

	protected $fillable = [];

	protected $rules = array(
		'name' => 'required|min:3',
		'phone' => 'required|min:8'
	);

	protected $messages = array(
		'name.required' => 'Lauks nosaukums ir obligāts',
		'name.min' => 'Nosaukuma minimālais garums ir :min simboli',
		'phone.required' => 'Lauks telefons ir obligāts',
		'phone.min' => 'Telefona minimālais garums ir :min simboli'
	);

}