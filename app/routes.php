<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Register all the admin routes.
|
*/

Route::group(array('prefix' => 'admin', 'before'=>'admin-auth'), function () {

    # User Management
    Route::group(array('prefix' => 'users'), function () {
        Route::get('/', array('as' => 'users', 'uses' => 'Controllers\Admin\UsersController@getIndex'));
        Route::get('create', array('as' => 'create/user', 'uses' => 'Controllers\Admin\UsersController@getCreate'));
        Route::post('create', 'Controllers\Admin\UsersController@postCreate');
        Route::get('{userId}/edit', array('as' => 'update/user', 'uses' => 'Controllers\Admin\UsersController@getEdit'));
        Route::post('{userId}/edit', 'Controllers\Admin\UsersController@postEdit');
        Route::get('{userId}/delete', array('as' => 'delete/user', 'uses' => 'Controllers\Admin\UsersController@getDelete'));
        Route::get('{userId}/confirm-delete', array('as' => 'confirm-delete/user', 'uses' => 'Controllers\Admin\UsersController@getModalDelete'));
        Route::get('{userId}/restore', array('as' => 'restore/user', 'uses' => 'Controllers\Admin\UsersController@getRestore'));
    });

    # Group Management
    Route::group(array('prefix' => 'groups'), function () {
        Route::get('/', array('as' => 'groups', 'uses' => 'Controllers\Admin\GroupsController@getIndex'));
        Route::get('create', array('as' => 'create/group', 'uses' => 'Controllers\Admin\GroupsController@getCreate'));
        Route::post('create', 'Controllers\Admin\GroupsController@postCreate');
        Route::get('{groupId}/edit', array('as' => 'update/group', 'uses' => 'Controllers\Admin\GroupsController@getEdit'));
        Route::post('{groupId}/edit', 'Controllers\Admin\GroupsController@postEdit');
        Route::get('{groupId}/delete', array('as' => 'delete/group', 'uses' => 'Controllers\Admin\GroupsController@getDelete'));
        Route::get('{groupId}/confirm-delete', array('as' => 'confirm-delete/group', 'uses' => 'Controllers\Admin\GroupsController@getModalDelete'));
        Route::get('{groupId}/restore', array('as' => 'restore/group', 'uses' => 'Controllers\Admin\GroupsController@getRestore'));
    });

    # Services management
    Route::group(array('prefix' => 'services'),function(){
        Route::get('/',array('as' => 'services', 'uses' => 'Controllers\Admin\ServicesController@getIndex'));
        //edit routes
        Route::get('{serviceID}/edit',array('as' => 'update/service', 'uses' => 'Controllers\Admin\ServicesController@getEdit'));
        Route::post('{serviceID}/edit','Controllers\Admin\ServicesController@postEdit');
        //create
        Route::post('create','Controllers\Admin\ServicesController@postCreate');
        //delete
        Route::get('{serviceID}/delete',array('as'=>'delete/service','uses'=>'Controllers\Admin\ServicesController@getDelete'));
        Route::get('{serviceID}/confirm-delete',array('as'=>'confirm-delete/service','uses'=>'Controllers\Admin\ServicesController@getModalDelete'));
        //test json
        Route::get('json',array('as'=>'json','uses' => 'Controllers\Admin\ServicesController@getServiceListJSON'));
    });

    # Institution management
    Route::group(array('prefix' => 'institutions'), function(){
        Route::get('/',array('as' => 'institutions', 'uses' => 'Controllers\Admin\InstitutionsController@getIndex'));
        //institution routes
        Route::get('{institutionID}/edit', array('as'=>'update/institution', 'uses'=>'Controllers\Admin\InstitutionsController@getEdit'));
        Route::post('{institutionID}/edit','Controllers\Admin\InstitutionsController@postEdit');
        Route::post('create','Controllers\Admin\InstitutionsController@postCreate');
        Route::get('{institutionID}/delete',array('as'=>'delete/institution','uses'=>'Controllers\Admin\InstitutionsController@getDelete'));
        Route::get('{institutionID}/confirm-delete',array('as'=>'confirm-delete/institution','uses'=>'Controllers\Admin\InstitutionsController@getModalDelete'));
        //weight
        Route::post('postWeight','Controllers\Admin\InstitutionsController@postInstitutionWeight');
        /* USERS */
        Route::get('{institutionID}/users',array('as'=>'institution/users','uses'=>'Controllers\Admin\InstitutionsController@getUserList'));
        //create
        Route::post('{institutionID}/users/create','Controllers\Admin\InstitutionsController@postUser');
        //edit
        Route::get('{institutionID}/users/{userID}/edit',array('as'=>'update/institutionUser','uses'=>'Controllers\Admin\InstitutionsController@getUserEdit'));
        Route::post('{institutionID}/users/{userID}/edit','Controllers\Admin\InstitutionsController@postUserEdit');
        //delete
        Route::get('{institutionID}/users/{userID}/delete',array('as'=>'delete/institutionUser','uses'=>'Controllers\Admin\InstitutionsController@getDeleteUser'));
        Route::get('{institutionID}/users/{userID}/confirm-delete',array('as'=>'confirm-delete/institutionUser','uses'=>'Controllers\Admin\InstitutionsController@getModalDeleteUser'));
        //user veight
        Route::post('postPersonWeight','Controllers\Admin\InstitutionsController@postPersonWeight');
        //test json for api
        Route::get('json',array('as'=>'json','uses'=>'Controllers\Admin\InstitutionsController@getInstitutionsJSON'));
    });

    # TAXI management
    Route::group(array('prefix' => 'taxi'),function(){
        Route::get('/', array('as'=>'taxi','uses'=>'Controllers\Admin\TaxiController@getIndex'));
        //create
        Route::post('create','Controllers\Admin\TaxiController@postCreate');
        //edit
        Route::get('{taxiID}/edit',array('as'=>'update/taxi','uses'=>'Controllers\Admin\TaxiController@getEdit'));
        Route::post('{taxiID}/edit','Controllers\Admin\TaxiController@postEdit');
        //delete
        Route::get('{taxiID}/delete',array('as'=>'delete/taxi','uses'=>'Controllers\Admin\TaxiController@getDelete'));
        Route::get('{taxiID}/confirm-delete',array('as'=>'confirm-delete/taxi','uses'=>'Controllers\Admin\TaxiController@getModalDelete'));
        //json test
        Route::get('json','Controllers\Admin\TaxiController@getJsonList');
    });

    # Restaurant management
    Route::group(array('prefix'=>'restaurants'),function(){
        Route::get('/',array('as'=>'restaurants','uses'=>'Controllers\Admin\RestaurantController@getIndex'));
        //create
        Route::post('create','Controllers\Admin\RestaurantController@postCreate');
        //edit
        Route::get('{restaurantID}/edit',array('as'=>'update/restaurant','uses'=>'Controllers\Admin\RestaurantController@getEdit'));
        Route::post('{restaurantID}/edit','Controllers\Admin\RestaurantController@postEdit');
        //delete
        Route::get('{restaurantID}/delete',array('as'=>'delete/restaurant','uses'=>'Controllers\Admin\RestaurantController@getDelete'));
        Route::get('{restaurantID}/confirm-delete',array('as'=>'confirm-delete/restaurant','uses'=>'Controllers\Admin\RestaurantController@getModalDelete'));
        //json test form web view
        Route::get('json','Controllers\Admin\RestaurantController@getJsonList');
    });

    #Hotel management
    Route::group(array('prefix'=>'hotels'),function(){
        Route::get('/',array('as'=>'hotels','uses'=>'Controllers\Admin\HotelController@getIndex'));
        //create
        Route::post('create','Controllers\Admin\HotelController@postCreate');
        //edit
        Route::get('{hotelID}/edit',array('as'=>'update/hotel','uses'=>'Controllers\Admin\HotelController@getEdit'));
        Route::post('{hotelID}/edit','Controllers\Admin\HotelController@postEdit');
        //delete
        Route::get('{hotelID}/delete',array('as'=>'delete/hotel','uses'=>'Controllers\Admin\HotelController@getDelete'));
        Route::get('{hotelID}/confirm-delete',array('as'=>'confirm-delete/hotel','uses'=>'Controllers\Admin\HotelController@getModalDelete'));
        //json test form web view
        Route::get('json','Controllers\Admin\HotelController@getJsonList');
    });

    #Busstop management
    Route::group(array('prefix'=>'busstops'),function(){
        Route::get('/',array('as'=>'busstops','uses'=>'Controllers\Admin\BusstopController@getIndex'));
        //map view
        Route::get('map','Controllers\Admin\BusstopController@getMapView');
        //create
        Route::get('create','Controllers\Admin\BusstopController@getCreate');
        Route::post('create','Controllers\Admin\BusstopController@postCreate');
        //edit
        Route::get('{busstopID}/edit',array('as'=>'update/busstop','uses'=>'Controllers\Admin\BusstopController@getEdit'));
        Route::post('{busstopID}/edit','Controllers\Admin\BusstopController@postEdit');
        //delete
        Route::get('{busstopID}/delete',array('as'=>'delete/busstop','uses'=>'Controllers\Admin\BusstopController@getDelete'));
        Route::get('{busstopID}/confirm-delete',array('as'=>'confirm-delete/busstop','uses'=>'Controllers\Admin\BusstopController@getModalDelete'));
        //delete file
        Route::get('{busstopID}/delete-file',array('as'=>'delete/busstop-file','uses'=>'Controllers\Admin\BusstopController@getDeleteFile'));
        //json test form web view
        Route::get('json','Controllers\Admin\BusstopController@getJsonList');
    });

    #Place management
    Route::group(array('prefix'=>'places'),function(){
        Route::get('/',array('as'=>'places','uses'=>'Controllers\Admin\PlaceController@getIndex'));
        //map view
        Route::get('map','Controllers\Admin\PlaceController@getMapView');
        //create
        Route::get('create','Controllers\Admin\PlaceController@getCreate');
        Route::post('create','Controllers\Admin\PlaceController@postCreate');
        //edit
        Route::get('{placeID}/edit',array('as'=>'update/place','uses'=>'Controllers\Admin\PlaceController@getEdit'));
        Route::post('{placeID}/edit','Controllers\Admin\PlaceController@postEdit');
        //delete place
        Route::get('{placeID}/delete',array('as'=>'delete/place','uses'=>'Controllers\Admin\PlaceController@getDelete'));
        Route::get('{placeID}/confirm-delete',array('as'=>'confirm-delete/place','uses'=>'Controllers\Admin\PlaceController@getModalDelete'));
        //post order
        Route::post('postOrder','Controllers\Admin\PlaceController@postOrder');
        //json test from web
        Route::get('json','Controllers\Admin\PlaceController@getJsonList');
    });

    #audioguide management
    Route::group(array('prefix'=>'audioguides'),function(){
        Route::get('/',array('as'=>'audioguides','uses'=>'Controllers\Admin\AudioguideController@getIndex'));
        //create
        Route::get('create','Controllers\Admin\AudioguideController@getCreate');
        Route::post('create','Controllers\Admin\AudioguideController@postCreate');
        //edit
        Route::get('{audioID}/edit',array('as'=>'update/audioguide','uses'=>'Controllers\Admin\AudioguideController@getEdit'));
        Route::post('{audioID}/edit','Controllers\Admin\AudioguideController@postEdit');
        //delete audioguide
        Route::get('{audioID}/delete',array('as'=>'delete/audioguides','uses'=>'Controllers\Admin\AudioguideController@getDelete'));
        Route::get('{audioID}/confirm-delete',array('as'=>'confirm-delete/audioguides','uses'=>'Controllers\Admin\AudioguideController@getModalDelete'));
        //delete audioguide file
        Route::get('{audioID}/{language}/delete-file',array('as'=>'delete/audioguide-file','uses'=>'Controllers\Admin\AudioguideController@getDeleteFile'));
        Route::get('{audioID}/{language}/confirm-file-delete',array('as'=>'confirm-file-delete/audioguides','uses'=>'Controllers\Admin\AudioguideController@getModalDeleteFile'));
        //json test from web
        Route::get('json','Controllers\Admin\AudioguideController@getJsonList');
    });

    # Dashboard
    Route::get('/', array('as' => 'admin', 'uses' => 'Controllers\Admin\DashboardController@getIndex'));

});

// api key authorization
Route::group(array('prefix' => 'api'), function(){
    //login
    Route::post('login',array('as' => 'login', 'uses' => 'AuthController@apiLogin'));
    //testing token auth
    Route::get('/', array('before'=>'auth.token',function(){
        //$user = Sentry::getUser();
        return Response::json(array('user' => 'No action specified'));
    }));
    //services list
    Route::get('/service-list',array('before'=>'auth.token','uses' => 'Controllers\Admin\ServicesController@getServiceListJSON'));
    //institution list
    Route::get('/institution-list',array('before'=>'auth.token','uses'=>'Controllers\Admin\InstitutionsController@getInstitutionsJSON'));
    //taxi list
    Route::get('/taxi-list-auth',array('before'=>'auth.token','uses'=>'Controllers\Admin\TaxiController@getJsonList'));
    Route::get('/taxi-list',array('uses'=>'Controllers\Admin\TaxiController@getJsonList'));
    //retaurant list
    Route::get('/restaurant-list',array('before'=>'auth.token','uses'=>'Controllers\Admin\RestaurantController@getJsonList'));
    //busstop list
    Route::get('/busstop-list', array('before'=>'auth.token','uses'=>'Controllers\Admin\BusstopController@getJsonList'));
    //place list
    Route::get('/place-list',array('before'=>'auth.token','uses'=>'Controllers\Admin\PlaceController@getJsonList'));
    //hotel list
    Route::get('/hotel-list',array('before'=>'auth.token','uses'=>'Controllers\Admin\HotelController@getJsonList'));
});

/*
|--------------------------------------------------------------------------
| Authentication and Authorization Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::group(array('prefix' => 'auth'), function () {

    # Login
    Route::get('signin', array('as' => 'signin', 'uses' => 'AuthController@getSignin'));
    Route::post('signin', 'AuthController@postSignin');

    # Register
    Route::get('signup', array('as' => 'signup', 'uses' => 'AuthController@getSignup'));
    Route::post('signup', 'AuthController@postSignup');

    # Account Activation
    Route::get('activate/{activationCode}', array('as' => 'activate', 'uses' => 'AuthController@getActivate'));

    # Forgot Password
    Route::get('forgot-password', array('as' => 'forgot-password', 'uses' => 'AuthController@getForgotPassword'));
    Route::post('forgot-password', 'AuthController@postForgotPassword');

    # Forgot Password Confirmation
    Route::get('forgot-password/{passwordResetCode}', array('as' => 'forgot-password-confirm', 'uses' => 'AuthController@getForgotPasswordConfirm'));
    Route::post('forgot-password/{passwordResetCode}', 'AuthController@postForgotPasswordConfirm');

    # Logout
    Route::get('logout', array('as' => 'logout', 'uses' => 'AuthController@getLogout'));

});

/*
|--------------------------------------------------------------------------
| Account Routes
|--------------------------------------------------------------------------
|
|
|
*/

Route::group(array('prefix' => 'account'), function () {

    # Account Dashboard
    Route::get('/', array('as' => 'account', 'uses' => 'Controllers\Account\DashboardController@getIndex'));

    # Profile
    Route::get('profile', array('as' => 'profile', 'uses' => 'Controllers\Account\ProfileController@getIndex'));
    Route::post('profile', 'Controllers\Account\ProfileController@postIndex');

    # Change Password
    Route::get('change-password', array('as' => 'change-password', 'uses' => 'Controllers\Account\ChangePasswordController@getIndex'));
    Route::post('change-password', 'Controllers\Account\ChangePasswordController@postIndex');

    # Change Email
    Route::get('change-email', array('as' => 'change-email', 'uses' => 'Controllers\Account\ChangeEmailController@getIndex'));
    Route::post('change-email', 'Controllers\Account\ChangeEmailController@postIndex');

});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('about-us', function () {
    //
    return View::make('frontend/about-us');
});

Route::get('contact-us', array('as' => 'contact-us', 'uses' => 'ContactUsController@getIndex'));
Route::post('contact-us', 'ContactUsController@postIndex');

Route::get('/', array('as' => 'signin', 'uses' => 'AuthController@getSignin'));
Route::get('signin', array('as' => 'signin', 'uses' => 'AuthController@getSignin'));
Route::post('signin', 'AuthController@postSignin');
