@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
<!-- MAIN TITLE HERE --> ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="row">
    <div class="col-xs-8">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title pull-left"><!-- KREISAA PUSE TITLE --></h3>
                <div class="box-tools pull-right"></div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <!-- KREISAA PUSE GALVENAIS CONTENT -->
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
    <!-- user form -->
    <div class="col-xs-4">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title"><!-- LABAA PUSE TITLE --></h3>
                <div class="box-tools"></div>
            </div><!-- /.box-header -->
            <div class="box-body">
            	<!-- LABAA PUSE CONTENT -->
            </div>
        </div>
    </div>
</div>

@stop

{{-- Body Bottom confirm modal --}}
@section('body_bottom')
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="institution_delete_confirm_title" aria-hidden="true">
  <div class="modal-dialog" style="background-color:#ffffff;">
    <div class="modal-content" style="background-color:#ffffff;">
    </div>
  </div>
</div>
<script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>

@stop