<?php

Validator::extend('alpha_space', function ($attribute,$value,$parameters) {
    return preg_match("/^[\s\n\-+:?#~'\/\(\)_,!.a-zA-Z0-9\pL\pN_-]+$/um",$value);
});

Validator::extend('mime-type',function($attribute,$value,$parameters){
	$mimes = Config::get('mimes');
	foreach($mimes as $mime=>$types){
		foreach((array) $types as $type){
			if($value['type']==$type && in_array($mime,$parameters)){
				return true;
			}
		}
	}
	return false;
});