@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Audiogida ieraksta izveidošana ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="row">
    <div class="col-xs-10 col-xs-offset-1">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title pull-left">Audiogida ieraksta izveidošana</h3>
                <div class="box-tools pull-right"></div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <form class="form-horizontal" name="audioguide" method="post" action="" role="form" enctype="multipart/form-data">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <!-- select place to assing -->
                    <div class="form-group {{ $errors->first('places_id','has-error') }}">
                        <label for="place" class="col-sm-3">Piesaistīt vietai</label>
                        <div class="col-sm-11">
                            <select name="places_id" id="place" class="form-control">
                                <option value="0">-- Nepiesaistīt --</option>
                                @if(count($placeList)>0)
                                @foreach($placeList as $placeInfo)
                                    <option value="{{{ $placeInfo->id }}}" {{ (Input::old('places_id')==$placeInfo->id) ? 'selected' : '' }}>
                                        {{{ $placeInfo->name_lv }}} - {{{ $placeInfo->name_en }}} - {{{$placeInfo->name_ru}}} - {{{$placeInfo->name_est}}}
                                    </option>
                                @endforeach
                                @endif
                            </select>
                        </div>
                    </div>
                    <!-- generate file fields -->
                    @if(count($languageList)>0)
                    @foreach($languageList as $language)
                    <div class="form-group {{ $errors->first('file-'.$language,'has-error') }}">
                        <label for="file-{{ $language }}" class="col-sm-3">Fails {{ strtoupper($language) }}</label>
                        <div class="col-sm-11">
                            <input type="file" id="file-{{$language}}" name="file-{{$language}}" />
                        </div>
                        <div class="col-sm-10">
                            {{ $errors->first('file-'.$language,'<span class="help-block">:message</span>') }}
                        </div>
                    </div>
                    @endforeach
                    @endif
                    <!-- form actions -->
                    <div class="form-group" style="margin-top:10px;">
                        <div class="col-xs-offset-9 col-xs-3">
                            <a class="btn btn-warning" href="{{ route('audioguides') }}">@lang('button.cancel')</a>
                            <button type="submit" class="btn btn-success">@lang('button.save')</button>
                        </div>
                    </div>
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

@stop