@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Audiogida saraksts ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="row">
    <div class="col-xs-10 col-xs-offset-1">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title pull-left">Audiogida saraksts</h3>
                <div class="box-tools pull-right">
                    <a href="{{ URL::to('admin/audioguides/create') }}" class="btn btn-success">Jauns ieraksts</a>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover table-striped" style="font-weight:normal">
                    <thead>
                        <th>Fails LV</th>
                        <th>Fails EN</th>
                        <th>Fails RU</th>
                        <th>Fails EST</th>
                        <th class="col-xs-1 text-center">Vieta</th>
                        <th class="col-xs-1"></th>
                    </thead>
                    <tbody>
                    @if(count($audioList)>0)
                    @foreach($audioList as $audioInfo)
                        <tr id="audio_{{{ $audioInfo->id }}}">
                            <td>
                                <a href="{{ $audioInfo->file_lv }}">{{{ $audioInfo->fileName('lv') }}}</a>
                                @if($audioInfo->file_lv != "")
                                    <a href="{{ route('confirm-file-delete/audioguides',array($audioInfo->id,'lv')) }}" data-toggle='modal' data-target="#delete_confirm"><i class="fa fa-trash-o text-red"></i></a>
                                @endif
                            </td>
                            <td>
                                <a href="{{ $audioInfo->file_en }}">{{{ $audioInfo->fileName('en') }}}</a>
                                @if($audioInfo->file_en != "")
                                    <a href="{{ route('confirm-file-delete/audioguides',array($audioInfo->id,'en')) }}" data-toggle='modal' data-target="#delete_confirm"><i class="fa fa-trash-o text-red"></i></a>
                                @endif
                            </td>
                            <td>
                                <a href="{{ $audioInfo->file_ru }}">{{{ $audioInfo->fileName('ru') }}}</a>
                                @if($audioInfo->file_ru != "")
                                    <a href="{{ route('confirm-file-delete/audioguides',array($audioInfo->id,'ru')) }}" data-toggle='modal' data-target="#delete_confirm"><i class="fa fa-trash-o text-red"></i></a>
                                @endif
                            </td>
                            <td>
                                <a href="{{ $audioInfo->file_est }}">{{{ $audioInfo->fileName('est') }}}</a>
                                @if($audioInfo->file_est != "")
                                    <a href="{{ route('confirm-file-delete/audioguides',array($audioInfo->id,'est')) }}" data-toggle='modal' data-target="#delete_confirm"><i class="fa fa-trash-o text-red"></i></a>
                                @endif
                            </td>
                            <td align="center">
                                <i class="fa fa-globe {{ (is_null($audioInfo->audioPlace) ? 'text-red' : 'text-green') }}"></i>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">Darbības</button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ route('update/audioguide', $audioInfo->id) }}"><i class="fa fa-edit"></i> Labot</a></li>
                                        <li>
                                        <a href="{{ route('confirm-delete/audioguides', $audioInfo->id) }}" data-toggle="modal" data-target="#delete_confirm"><i class="fa fa-trash-o"></i> Dzēst</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    @else
                    <tr>
                        <td colspan="6">Nav pievienots neviens ieraksts</td>
                    </tr>
                    @endif
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

@stop

{{-- Body Bottom confirm modal --}}
@section('body_bottom')
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="institution_delete_confirm_title" aria-hidden="true">
  <div class="modal-dialog" style="background-color:#ffffff;">
    <div class="modal-content" style="background-color:#ffffff;">
    </div>
  </div>
</div>
<script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>

@stop