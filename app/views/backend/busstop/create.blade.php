@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Izveidot pieturas ierakstu ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="row">
    <div class="col-xs-9 col-xs-offset-1">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title pull-left">Jauns pieturas ieraksts</h3>
                <div class="box-tools pull-right"></div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <form class="form-horizontal" name="createBusstop" method="post" action="" role="form" enctype="multipart/form-data">
                    <!-- CSRF Token -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <!-- Name -->
                    <div class="form-group {{ $errors->first('name', 'has-error') }}">
                        <label for="name" class="col-sm-3">Nosaukums</label>
                        <div class="col-sm-11">
                            <input type="text" id="name" name="name" class="form-control" placeholder="Nosaukums" value="{{{ Input::old('name') }}}">
                        </div>
                        <div class="col-sm-10">
                            {{ $errors->first('name', '<span class="help-block">:message</span>') }}
                        </div>
                    </div>
                    <!-- file -->
                    <div class="form-group {{ $errors->first('file','has-error') }}">
                        <label for="file" class="col-sm-3">Fails</label>
                        <div class="col-sm-11">
                            <input type="file" id="file" name="file" />
                        </div>
                    </div>
                    <!-- place -->
                    <div class="form-group">
                        <label for="latlng" class="col-sm-3">Vieta</label>
                        <div class="col-sm-12">
                            <input type="text" name="latlng" id="latlng" class="form-control input-sm" value="" readonly="readonly" />
                            <div id="mapBox" style="width:100%;height:350px"></div>
                        </div>
                    </div>
                    <!-- Form Actions -->
                    <div class="form-group" style="margin-top:10px;">
                        <div class="col-xs-offset-9 col-xs-3">
                            <a class="btn btn-warning" href="{{ route('busstops') }}">@lang('button.cancel')</a>
                            <button type="submit" class="btn btn-success">@lang('button.save')</button>
                        </div>
                    </div>
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

@stop

{{-- Body Bottom confirm modal --}}
@section('body_bottom')
{{-- Sheit viss kas attiecas uz google map --}}
<script type="text/javascript">
var map, mapListener;
var marker = null;
$(function () {
    function initMapBox(){
        var mapOptions = {
            center: new google.maps.LatLng(57.538466, 25.426362),
            zoom: 14,
            panControl: false,
            zoomControl: true,
            scaleControl: false,
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("mapBox"),mapOptions);
        setMapBoxListener();
        //google.maps.event.removeListener(mapListener);
        google.maps.event.addListenerOnce(map,'idle',function(){
            map.fitBounds(this.getBounds());
        });
    }

    function setMapBoxListener(){
        mapListener = google.maps.event.addListener(map,'click',function(event){
            //remove all markers and reset coordinate collector
            if(marker !== null){
                marker.setMap(null);
                marker = null;
            }
            $("#latlng").val("");
            //create marker add listener
            marker = new google.maps.Marker({
                position: event.latLng,
                map: map,
                draggable: true
            });
            map.setCenter(event.latLng);
            $("#latlng").val(event.latLng);

            google.maps.event.addListener(marker,'dragend',function(){
                $("#latlng").val(marker.getPosition());
            });

        });
    }
    initMapBox();
});
</script>

@stop