@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Labot pieturas ierakstu ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="row">
    <div class="col-xs-9 col-xs-offset-1">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title pull-left">Labot pieturas ierakstu</h3>
                <div class="box-tools pull-right"></div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <form class="form-horizontal" name="createBusstop" method="post" action="" role="form" enctype="multipart/form-data">
                    <!-- CSRF Token -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <!-- Name -->
                    <div class="form-group {{ $errors->first('name', 'has-error') }}">
                        <label for="name" class="col-sm-3">Nosaukums</label>
                        <div class="col-sm-11">
                            <input type="text" id="name" name="name" class="form-control" placeholder="Nosaukums" value="{{{ Input::old('name',$busstop->name) }}}">
                        </div>
                        <div class="col-sm-10">
                            {{ $errors->first('name', '<span class="help-block">:message</span>') }}
                        </div>
                    </div>
                    <!-- file -->
                    <div class="form-group {{ $errors->first('file','has-error') }}">
                        <label for="file" class="col-sm-3">Fails</label>
                        <div class="col-sm-11">
                            <input type="file" id="file" name="file" />
                        </div>
                        <div class="col-sm-11">
                            Iepriekšējais fails: <a href="{{ $busstop->file }}">{{{ $busstop->fileName() }}}</a>
                            <a href="{{ route('delete/busstop-file',$busstop->id) }}" class="btm btn-sm btn-danger">
                                <i class="fa fa-trash-o"></i> Dzēst failu
                            </a>
                        </div>
                    </div>
                    <!-- place -->
                    <div class="form-group">
                        <label for="latlng" class="col-sm-3">Vieta</label>
                        <div class="col-sm-12">
                            <input type="text" name="latlng" id="latlng" class="form-control input-sm" value="{{{ $busstop->latlng }}}" readonly="readonly" />
                            <div id="mapBox" style="width:100%;height:350px"></div>
                        </div>
                    </div>
                    <!-- Form Actions -->
                    <div class="form-group" style="margin-top:10px;">
                        <div class="col-xs-offset-9 col-xs-3">
                            <a class="btn btn-warning" href="{{ route('busstops') }}">@lang('button.cancel')</a>
                            <button type="submit" class="btn btn-success">@lang('button.save')</button>
                        </div>
                    </div>
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

@stop

{{-- Body Bottom confirm modal --}}
@section('body_bottom')
{{-- Sheit viss kas attiecas uz google map --}}
<script type="text/javascript">
var map;
var marker = null;
$(function () {
    function initMapBox(){
        var mapOptions = {
            center: new google.maps.LatLng(57.538466, 25.426362),
            zoom: 14,
            panControl: false,
            zoomControl: true,
            scaleControl: false,
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("mapBox"),mapOptions);
        google.maps.event.addListenerOnce(map,'idle',function(){
            map.fitBounds(this.getBounds());
        });
        //add marker
        marker = new google.maps.Marker({
            position: new google.maps.LatLng({{ $busstop->latlng }}),
            map: map,
            draggable: true
        });
        google.maps.event.addListener(marker,'dragend',function(){
            $("#latlng").val(marker.getPosition());
        });
    }
    initMapBox();
});
</script>

@stop