@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Autobusu pieturu saraksts ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="row">
    <div class="col-xs-9 col-xs-offset-1">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title pull-left">Autobusu pieturu saraksts</h3>
                <div class="box-tools pull-right">
                    <a href="{{ URL::to('admin/busstops/create') }}" class="btn btn-success">
                        Jauna pietura
                    </a>
                    <a href="{{ URL::to('admin/busstops/map') }}" class="btn btn-info">
                        Pieturas kartē
                    </a>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr>
                            <th>Nosaukums</th>
                            <th>Fails</th>
                            <th></th>
                        </tr>
                        @if(count($busstopList) > 0)
                        @foreach($busstopList as $busstop)
                        <tr>
                            <td>{{{ $busstop->name }}}</td>
                            <td>
                                <a href="{{ $busstop->file }}">{{{ $busstop->fileName() }}}</a>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">Darbības</button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ route('update/busstop', $busstop->id) }}"><i class="fa fa-edit"></i> Labot</a></li>
                                        <li>
                                        <a href="{{ route('confirm-delete/busstop', $busstop->id) }}" data-toggle="modal" data-target="#delete_confirm"><i class="fa fa-trash-o"></i> Dzēst</a></li>
                                        <li>
                                            <a href="{{ route('delete/busstop-file',$busstop->id) }}">
                                                <i class="fa fa-trash-o"></i> Dzēst failu
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="3">Nav pievienots neviens ieraksts</td>
                        </tr>
                        @endif
                    </thead>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

@stop

{{-- Body Bottom confirm modal --}}
@section('body_bottom')
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="busstop_delete_confirm_title" aria-hidden="true">
  <div class="modal-dialog" style="background-color:#ffffff;">
    <div class="modal-content" style="background-color:#ffffff;">
    </div>
  </div>
</div>
<script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>

@stop