@extends('frontend/layouts/default')

@section('title')
Darbavirsma ::
@parent
@stop

@section('content')
<div class="row">
	<!-- left side -->
	<div class="col-xs-7">
		<!-- dashboard icons to modules -->
		<div class="box box-primary">
	        <div class="box-header">
	            <h3 class="box-title text-blue">Darbavirsma</h3>
	            <div class="box-tools"></div>
	        </div><!-- /.box-header -->
	        <div class="box-body">
                @if(Sentry::check() && Sentry::getUser()->hasAccess('places.read'))
	            <a href="{{ URL::to('admin/places') }}" class="btn btn-app">
                    <i class="fa fa-globe"></i> Vietas
                </a>
                @endif
                @if(Sentry::check() && Sentry::getUser()->hasAccess('services.read'))
                <a href="{{ URL::to('admin/services') }}" class="btn btn-app">
                	<i class="fa fa-gear"></i> Dienesti
                </a>
                @endif
                @if(Sentry::check() && Sentry::getUser()->hasAccess('municipality.read'))
                <a href="{{ URL::to('admin/institutions') }}" class="btn btn-app">
                	<i class="fa fa-building-o"></i> Pašvaldība
                </a>
                @endif
                @if(Sentry::check() && Sentry::getUser()->hasAccess('busstops.read'))
                <a href="{{ URL::to('admin/busstops') }}" class="btn btn-app">
                	<i class="fa fa-road"></i> Pieturas
                </a>
                @endif
                @if(Sentry::check() && Sentry::getUser()->hasAccess('taxi.read'))
                <a href="{{ URL::to('admin/taxi') }}" class="btn btn-app">
                	<i class="fa fa-truck"></i> Taksometri
                </a>
                @endif
                @if(Sentry::check() && Sentry::getUser()->hasAccess('audioguide.read'))
                <a href="{{ URL::to('admin/audioguides') }}" class="btn btn-app">
                	<i class="fa fa-phone"></i> Audio gids
                </a>
                @endif
                <a href="#" class="btn btn-app">
                	<i class="fa fa-picture-o"></i> Galerija
                </a>
                @if(Sentry::check() && Sentry::getUser()->hasAccess('restaurants.read'))
                <a href="{{ URL::to('admin/restaurants') }}" class="btn btn-app">
                	<i class="fa fa-cutlery"></i> Kur paēst?
                </a>
                @endif
                @if(Sentry::check() && Sentry::getUser()->hasAccess('hotels.read'))
                <a href="{{ URL::to('admin/hotels') }}" class="btn btn-app">
                	<i class="fa fa-home"></i> Naktsmītnes
                </a>
                @endif
                @if(Sentry::check() && Sentry::getUser()->hasAccess('superadmin'))
                <a href="{{ URL::to('admin/users') }}" class="btn btn-app bg-blue">
                	<i class="fa fa-user"></i> Lietotāji
                </a>
                <a href="{{ URL::to('admin/groups') }}" class="btn btn-app bg-blue">
                	<i class="fa fa-users"></i> Grupas
                </a>
                @endif
	        </div>
	    </div>
	    <!-- test google maps box -->
	    <div class="box box-success">
            <div class="box-header">
                <i class="fa fa-cloud text-green"></i>
                <h3 class="box-title text-green">Karte</h3>
            </div>
            <div class="box-body no-padding">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="google-map" id="google-map" style="height: 600px;">
                        	
                        </div>
                    </div>
                </div>
            </div>
        </div>
	</div>

	<!-- right side -->
	<div class="col-xs-5">
        <!-- statistics view -->
        <div class="box box-danger">
            <div class="box-header">
                <div class="pull-right box-tools">
                    <button class="btn btn-danger btn-sm" data-widget='collapse' data-toggle="tooltip" title="Sakļaut"><i class="fa fa-minus"></i></button>
                </div>
                <i class="fa fa-cloud text-red"></i>
                <h3 class="box-title text-red">Statistika</h3>
            </div>
            <div class="box-body no-padding">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="chart" id="bar-chart" style="height: 250px;"></div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
@stop

@section('body_bottom')

<!-- testing map -->
<script type="text/javascript">
  function initialize() {
    var mapOptions = {
    	center: new google.maps.LatLng(57.538466, 25.426362),
    	zoom: 14,
    	panControl: false,
    	zoomControl: true,
    	scaleControl: false,
    	mapTypeControl: false,
    	mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    var map = new google.maps.Map(document.getElementById("google-map"),mapOptions);
  }
  google.maps.event.addDomListener(window, 'load', initialize);
</script>

<!-- testing statistics -->
<script type="text/javascript">
	//Bar chart
	$(function() {
    var bar = new Morris.Bar({
        element: 'bar-chart',
        resize: true,
        data: [
            {y: 'Ierīces', a: 122, b: 90, c: 80},
        ],
        barColors: ['#00a65a', '#f56954'],
        xkey: 'y',
        ykeys: ['a', 'b','c'],
        labels: ['Android', 'iOS6','iOS7'],
        hideHover: false
    });
});
</script>
@stop
