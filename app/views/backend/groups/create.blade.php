
<!-- Tabs -->
<div class="nav-tabs-custom">
<ul class="nav nav-tabs">
    <li class="active"><a href="#tab-general" data-toggle="tab">Pamata info</a></li>
    <li><a href="#tab-permissions" data-toggle="tab">Tiesības</a></li>
</ul>
</div>

<form class="form-horizontal" role="form" method="post" action="{{ URL::to('admin/groups/create') }}">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

    <!-- Tabs Content -->
    <div class="tab-content">
        <!-- General tab -->
        <div class="tab-pane active" id="tab-general">
        <br>
            <!-- Name -->
            <div class="form-group {{ $errors->first('name', 'has-error') }}">
                <label for="title" class="col-sm-2 control-label">Nosaukums</label>
                    <div class="col-sm-5">
                        <input type="text" id="name" name="name" class="form-control" placeholder="Nosaukums" value="{{{ Input::old('name') }}}">
                    </div>
                    <div class="col-sm-4">
                        {{ $errors->first('name', '<span class="help-block">:message</span>') }}
                    </div>
            </div>
        </div>

        <!-- Tab Permissions -->
        <div class="tab-pane" id="tab-permissions">
            <div class="control-group">
                <div class="controls">

                    @foreach ($permissions as $area => $permissions)
                    <fieldset>
                        <legend>{{ $area }}</legend>

                        @foreach ($permissions as $permission)
                        <div class="control-group row">
                            <div class="col-xs-3">
                                <label class="control-group">{{ $permission['label'] }}</label>
                            </div>
                            <div class="col-xs-2">
                                <div class="radio inline">
                                    <label for="{{{ $permission['permission'] }}}_allow" onclick="">
                                        <input type="radio" value="1" id="{{{ $permission['permission'] }}}_allow" name="permissions[{{{ $permission['permission'] }}}]"{{ (array_get($selectedPermissions, $permission['permission']) === 1 ? ' checked="checked"' : '') }}>
                                        Atļaut
                                    </label>
                                </div>
                            </div>
                            <div class="col-xs-2">
                                <div class="radio inline">
                                    <label for="{{{ $permission['permission'] }}}_deny" onclick="">
                                        <input type="radio" value="0" id="{{ $permission['permission'] }}_deny" name="permissions[{{ $permission['permission'] }}]"{{ ( ! array_get($selectedPermissions, $permission['permission']) ? ' checked="checked"' : '') }}>
                                        Aizliegt
                                    </label>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </fieldset>
                    @endforeach

                </div>
            </div>
        </div>
    </div>
    <!-- Form actions -->
    <div class="form-group" style="margin-top:10px;">
        <div class="col-xs-offset-8 col-xs-4">
            <a class="btn btn-warning" href="{{ route('groups') }}">@lang('button.cancel')</a>
            <button type="submit" class="btn btn-success">@lang('button.save')</button>
        </div>
    </div>
</form>
