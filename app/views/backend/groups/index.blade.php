@extends('backend/layouts/default')

{{-- Web site Title --}}
@section('title')
Grupu pārvaldība ::
@parent
@stop

{{-- Content --}}
@section('content')
<div class="row">
    <div class="col-xs-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Grupu saraksts</h3>
                <div class="box-tools"></div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th class="span6">@lang('admin/groups/table.name')</th>
                            <th class="span2">@lang('admin/groups/table.users')</th>
                            <th class="span2">@lang('admin/groups/table.created_at')</th>
                            <th class="span1"></th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($groups as $group)
                        <tr>
                            <td>{{{ $group->name }}}</td>
                            <td>{{{ $group->users()->count() }}}</td>
                            <td>{{{ $group->created_at->diffForHumans() }}}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">Darbības</button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ route('update/group', $group->id) }}"><span class="glyphicon glyphicon-pencil"></span> Labot</a></li>
                                        <li><a href="{{ route('confirm-delete/group', $group->id) }}" data-toggle="modal" data-target="#delete_confirm"><span class="glyphicon glyphicon-trash"></span> Dzēst</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="box-footer">
                {{ $groups->links() }}
            </div>
        </div>
    </div>
    <!-- group form -->
    <div class="col-xs-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Jauna grupa</h3>
                <div class="box-tools"></div>
            </div><!-- /.box-header -->
            <div class="box-body">
                @include('backend/groups/create',array('permissions'=>$permissions,'selectedPermissions'=>$selectedPermissions))
            </div>
        </div>
    </div>
</div>
@stop

{{-- Body Bottom confirm modal --}}
@section('body_bottom')
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
    </div>
  </div>
</div>
<script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
@stop
