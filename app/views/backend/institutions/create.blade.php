<form class="form-horizontal" role="form" method="post" action="{{ URL::to('admin/institutions/create') }}">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <!-- Name -->
        <div class="form-group {{ $errors->first('name', 'has-error') }}">
                <!--<label for="name" class="col-sm-10 control-label">Nosaukums</label> -->
                <div class="col-sm-12">
                    <input type="text" id="name" name="name" class="form-control" placeholder="Nosaukums" value="{{{ Input::old('name') }}}">
                </div>
                <div class="col-sm-12">
                    {{ $errors->first('name', '<span class="help-block">:message</span>') }}
                </div>
        </div>
    <!-- Form Actions -->
    <div class="form-group" style="margin-top:10px;">
        <div class="col-xs-offset-6 col-xs-6">
            <a class="btn btn-warning" href="{{ route('institutions') }}">@lang('button.cancel')</a>
            <button type="submit" class="btn btn-success">@lang('button.save')</button>
        </div>
    </div>
</form>