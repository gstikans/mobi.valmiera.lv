@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Struktūrvienības atjaunināšana ::
@parent
@stop

{{-- Page content --}}
@section('content')

<div class="col-xs-6">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Struktūrvienības atjaunināšana</h3>
        </div>
        <form class="form-horizontal" role="form" method="post" action="">
            <div class="box-body">
            <!-- CSRF Token -->
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <!-- Name -->
                <div class="form-group {{ $errors->first('name', 'has-error') }}">
                    <label for="name" class="col-sm-2 control-label">Nosaukums</label>
                        <div class="col-sm-5">
                            <input type="text" id="name" name="name" class="form-control" placeholder="Nosaukums" value="{{{ Input::old('name', $institution->name) }}}">
                        </div>
                        <div class="col-sm-4">
                            {{ $errors->first('name', '<span class="help-block">:message</span>') }}
                        </div>
                </div>
            <!-- Form Actions -->
            </div>
            <div class="box-footer">
                <div class="form-group" style="margin-top:10px;">
                    <div class="col-xs-offset-8 col-xs-4">
                        <a class="btn btn-warning" href="{{ route('institutions') }}">@lang('button.cancel')</a>
                        <button type="submit" class="btn btn-success">@lang('button.save')</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop