@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Pašvaldības pārvaldība ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="row">
    <div class="col-xs-8">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title pull-left">Struktūrvienību saraksts</h3>
                <div class="box-tools pull-right">
                    <div class="input-group">
                        <input type="text" name="table_search" data-column="2" class="form-control input-sm pull-right" placeholder="Filtrēt">
                    </div>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover table-striped" id="institutionTable">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Nosaukums</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($institutions)>0)
                            @foreach($institutions as $institution)
                                <tr id="institution_{{{ $institution->id }}}">
                                    <td><i class="fa fa-arrows handle" style="cursor:pointer;"></i></td>
                                    <td>{{{ $institution->name }}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">Darbības</button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="{{ route('institution/users', $institution->id) }}">
                                                        <i class="fa fa-user"></i> Personas
                                                    </a>
                                                </li>
                                                <li><a href="{{ route('update/institution', $institution->id) }}"><span class="glyphicon glyphicon-pencil"></span> Labot</a></li>
                                                <li>
                                                <a href="{{ route('confirm-delete/institution', $institution->id) }}" data-toggle="modal" data-target="#delete_confirm"><span class="glyphicon glyphicon-trash"></span> Dzēst</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                    <tbody>
                        
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
    <!-- user form -->
    <div class="col-xs-4">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Jauna struktūrvienība</h3>
                <div class="box-tools"></div>
            </div><!-- /.box-header -->
            <div class="box-body">
                @include('backend/institutions/create')
            </div>
        </div>
    </div>
</div>

@stop

{{-- Body Bottom confirm modal --}}
@section('body_bottom')
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="institution_delete_confirm_title" aria-hidden="true">
  <div class="modal-dialog" style="background-color:#ffffff;">
    <div class="modal-content" style="background-color:#ffffff;">
    </div>
  </div>
</div>
<script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
{{-- Make ajax managed ordering of table rows --}}
<script type="text/javascript">
function postInstitutionWeight(data){
    $.ajax({
        type: 'POST',
        url: "{{ URL::to('admin/institutions/postWeight') }}",
        data: data,
        success: function(d){},
        dataType: 'json'
    });
    $("#institutionTable").removeClass('table-striped').addClass('table-striped');
}
$(function(){
    $("#institutionTable tbody").makeSortable({sortCallBack:'postInstitutionWeight',handle:'.handle'});
    //table search
    var visibleRows = $("#institutionTable tbody tr:visible").size();
    var bodyRows = $("#institutionTable tbody tr");
    var inputElement = $("input[name=table_search]");
    inputElement.on('keyup',function(){
        if($(this).val()!==""){
            var elementValue = $(this).val().toLowerCase();
            bodyRows.each(function(){
                $(this).show();
                var $_column = $(':nth-child('+inputElement.data('column')+')',this).html().toLowerCase();
               // alert($_column);
                if($_column.match(""+elementValue+"")==null){
                    $(this).hide();
                }
            });
        }else{
            bodyRows.hide();
            bodyRows.slice(0,visibleRows).show();
        }
    });
});
</script>

@stop
