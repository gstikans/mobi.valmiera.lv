<form class="form-horizontal" role="form" method="post" action="{{ URL::to('admin/institutions/'.$institution->id.'/users/create') }}">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <!-- Name -->
        <div class="form-group {{ $errors->first('name', 'has-error') }}">
            <!--<label for="name" class="col-sm-5 control-label">Vārds, uzvārds</label>-->
            <div class="col-sm-12">
                <input type="text" id="name" name="name" class="form-control" placeholder="Vārds, uzvārds" value="{{{ Input::old('name') }}}">
            </div>
            <div class="col-sm-12">
                {{ $errors->first('name', '<span class="help-block">:message</span>') }}
            </div>
        </div>
        <!-- Job -->
        <div class="form-group {{ $errors->first('job', 'has-error') }}">
            <!--<label for="job" class="col-sm-5 control-label">Amats</label>-->
            <div class="col-sm-12">
                <input type="text" id="job" name="job" class="form-control" placeholder="Amats" value="{{{ Input::old('job') }}}">
            </div>
            <div class="col-sm-12">
                {{ $errors->first('job', '<span class="help-block">:message</span>') }}
            </div>
        </div>
        <!-- Mail -->
        <div class="form-group {{ $errors->first('mail', 'has-error') }}">
            <!--<label for="mail" class="col-sm-5 control-label">E-pasts</label>-->
            <div class="col-sm-12">
                <input type="text" id="mail" name="mail" class="form-control" placeholder="E-pasts" value="{{{ Input::old('mail') }}}">
            </div>
            <div class="col-sm-12">
                {{ $errors->first('mail', '<span class="help-block">:message</span>') }}
            </div>
        </div>
        <!-- Phone -->
        <div class="form-group {{ $errors->first('phone', 'has-error') }}">
            <!--<label for="phone" class="col-sm-5 control-label">Telefons</label>-->
            <div class="col-sm-12">
                <input type="text" id="phone" name="phone" class="form-control" placeholder="Telefons" value="{{{ Input::old('phone') }}}">
            </div>
            <div class="col-sm-12">
                {{ $errors->first('phone', '<span class="help-block">:message</span>') }}
            </div>
        </div>
        <!-- Institution -->
        <div class="form-group {{ $errors->first('institution_id', 'has-error') }}">
            <!--<label for="institution_id" class="col-sm-5 control-label">Telefons</label>-->
            <div class="col-sm-12">
                <select id="institution_id" name="institution_id" class="form-control">
                    @if(count($institutionList) > 0)
                        @foreach($institutionList as $institutionInfo)
                            <option value="{{{ $institutionInfo->id }}}" {{ (Input::old('institution_id',$institutionInfo->id) === $institution->id ? 'selected="selected"' : '') }}>{{{ $institutionInfo->name }}}</option>
                        @endforeach
                    @endif
                </select>
            </div>
            <div class="col-sm-12">
                {{ $errors->first('institution_id', '<span class="help-block">:message</span>') }}
            </div>
        </div>
    <!-- Form Actions -->
    <div class="form-group" style="margin-top:10px;">
        <div class="col-xs-offset-6 col-xs-6">
            <a class="btn btn-warning" href="{{ route('institution/users',$institution->id) }}">@lang('button.cancel')</a>
            <button type="submit" class="btn btn-success">@lang('button.save')</button>
        </div>
    </div>
</form>
