@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Struktūrvienības {{{ $institution->name }}} personas ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="row">
    <div class="col-xs-8">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title pull-left">{{{ $institution->name }}} - personu saraksts</h3>
                <div class="box-tools pull-right">
                    <a href="{{ URL::to('admin/institutions') }}" class="btn btn-warning btn-sm">
                        <i class="fa fa-arrow-left"></i> Atpakaļ
                    </a>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover table-striped" id="personTable">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Vārds, uzvārds</th>
                            <th>Amats</th>
                            <th>E-pasts</th>
                            <th>Telefons</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($userList)>0)
                            @foreach($userList as $user)
                                <tr id="user_{{{ $user->id }}}">
                                    <td><i class="fa fa-arrows handle" style="cursor:pointer;"></i></td>
                                    <td>{{{ $user->name }}}</td>
                                    <td>{{{ $user->job }}}</td>
                                    <td>{{{ $user->mail }}}</td>
                                    <td>{{{ $user->phone() }}}</td>
                                    <td>
                                       <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">Darbības</button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="{{ route('update/institutionUser', array($institution->id,$user->id)) }}"><span class="glyphicon glyphicon-pencil"></span> Labot</a></li>
                                                <li>
                                                <a href="{{ route('confirm-delete/institutionUser', array($institution->id,$user->id)) }}" data-toggle="modal" data-target="#delete_confirm"><span class="glyphicon glyphicon-trash"></span> Dzēst</a></li>
                                            </ul>
                                        </div> 
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
    <!-- user form -->
    <div class="col-xs-4">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Jauna persona</h3>
                <div class="box-tools"></div>
            </div><!-- /.box-header -->
            <div class="box-body">
                @include('backend/institutions/user_create',array('institution'=>$institution,'institutionList'=>$institutionList))
            </div>
        </div>
    </div>
</div>

@stop

{{-- Body Bottom confirm modal --}}
@section('body_bottom')
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="institution_delete_confirm_title" aria-hidden="true">
  <div class="modal-dialog" style="background-color:#ffffff;">
    <div class="modal-content" style="background-color:#ffffff;">
    </div>
  </div>
</div>
<script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
{{-- Make ajax managed ordering of table rows --}}
<script type="text/javascript">
function postPersonWeight(data){
    $.ajax({
        type: 'POST',
        url: "{{ URL::to('admin/institutions/postPersonWeight') }}",
        data: data,
        success: function(d){},
        dataType: 'json'
    });
    $("#personTable").removeClass('table-striped').addClass('table-striped');
}
$(function(){
    $("#personTable tbody").makeSortable({sortCallBack:'postPersonWeight',handle:'.handle'});
});
</script>

@stop
