@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Izveidot vietas ierakstu ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="row">
    <div class="col-xs-9 col-xs-offset-1">
        <div class="box">
            <div class="box-body" style="font-weight:normal;padding-bottom:30px;">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs pull-right">
                        @if(count($languageList)>0)
                        @foreach(array_reverse($languageList,true) as $k=>$language)
                        <li><a href="#tab_{{{ $language }}}" data-toggle="tab">{{ strtoupper($language) }}</a></li>
                        @endforeach
                        <li class="active"><a href="#tab_map" data-toggle="tab">Vieta</a></li>
                        @endif
                        <!-- title -->
                        <li class="pull-left header">Jauns vietas ieraksts</li>
                    </ul>
                    <form class="form-horizontal" name="createPlace" method="post" action="" role="form">
                    <div class="tab-content row" style="margin-bottom:10px;">
                        <div class="tab-pane active" id="tab_map">
                            <div class="col-sm-11" style="margin:5px;">
                                <p class="help-block">Nepieciešams norādīt vietu kartē</p>
                                <input type="text" name="latlng" id="latlng" class="form-control input-sm" value="" readonly="readonly" />
                                <div id="mapBox" style="width:100%;height:350px;"></div>
                            </div>
                        </div>
                        <!-- language panes -->
                        @if(count($languageList)>0)
                        @foreach($languageList as $k=>$language)
                        <div class="tab-pane" id="tab_{{{ $language }}}">
                            <div class="form-group {{ $errors->first('name_'.$language,'has-error') }} col-sm-11" style="margin:5px;">
                                <label for="name_{{$language}}" class="col-sm-3">Nosaukums {{ strtoupper($language) }}</label>
                                <div class="col-sm-11">
                                    <input type="text" name="name_{{$language}}" id="name_{{$language}}" class="form-control" placeholder="Nosaukums {{ strtoupper($language) }}" value="{{{ Input::old('name_'.$language) }}}">
                                </div>
                                <div class="col-sm-10">
                                    {{ $errors->first('name_'.$language,'<span class="help-block">:message</span>') }}
                                </div>
                                <label for="about_{{$language}}" class="col-sm-3">Apraksts {{ strtoupper($language) }}</label>
                                <div class="col-sm-11">
                                    <textarea name="about_{{$language}}" id="about_{{$language}}" class="form-control about-field" rows="5">{{{ Input::old('about_'.$language) }}}</textarea>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        @endif
                    </div><!-- /.tab-content -->
                    <div class="pull-right" style="margin-bottom:20px;">
                        <a class="btn btn-warning" href="{{ route('places') }}">@lang('button.cancel')</a>
                        <button type="submit" class="btn btn-success">@lang('button.save')</button>
                    </div>
                    </form>
                </div><!-- /.tabed-end -->
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>
@stop

{{-- Body Bottom confirm modal --}}
@section('body_bottom')
{{-- Sheit viss kas attiecas uz google map --}}
<script type="text/javascript">
var map, mapListener;
var marker = null;
$(function () {
    function initMapBox(){
        var mapOptions = {
            center: new google.maps.LatLng(57.538466, 25.426362),
            zoom: 14,
            panControl: false,
            zoomControl: true,
            scaleControl: false,
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("mapBox"),mapOptions);
        setMapBoxListener();
        //google.maps.event.removeListener(mapListener);
        google.maps.event.addListenerOnce(map,'idle',function(){
            map.fitBounds(this.getBounds());
        });
    }

    function setMapBoxListener(){
        mapListener = google.maps.event.addListener(map,'click',function(event){
            //remove all markers and reset coordinate collector
            if(marker !== null){
                marker.setMap(null);
                marker = null;
            }
            $("#latlng").val("");
            //create marker add listener
            marker = new google.maps.Marker({
                position: event.latLng,
                map: map,
                draggable: true
            });
            map.setCenter(event.latLng);
            $("#latlng").val(event.latLng);

            google.maps.event.addListener(marker,'dragend',function(){
                $("#latlng").val(marker.getPosition());
            });

        });
    }
    initMapBox();
    $(".about-field").wysihtml5({
        "font-styles": false,
        "lists": false,
        "link": false, //Button to insert a link. Default true
        "image": false,
        "color": false 
    });
});
</script>
@stop