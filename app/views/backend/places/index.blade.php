@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Vietu saraksts ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="row">
    <div class="col-xs-10 col-xs-offset-1">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title pull-left">Vietu saraksts</h3>
                <div class="box-tools pull-right">
                    <a href="{{ URL::to('admin/places/create') }}" class="btn btn-success">Jauna vieta</a>
                    <a href="{{ URL::to('admin/places/map') }}" class="btn btn-info">Karte</a>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover table-striped" style="font-weight:normal" id="placeTable">
                    <thead>
                        <tr>
                            <th></th>
                            <th>Nosaukums LV</th>
                            <th>Nosaukums EN</th>
                            <th>Nosaukums RU</th>
                            <th>Nosaukums EST</th>
                            <th class="col-xs-1 text-center">Galerija</th>
                            <th class="col-xs-1 text-center">Audio</th>
                            <th class="col-xs-1"></th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(count($placeList)>0)
                    @foreach($placeList as $placeInfo)
                        <tr id="place_{{{ $placeInfo->id }}}">
                            <td><i class="fa fa-arrows handle" style="cursor:pointer;"></i></td>
                            <td>{{{ $placeInfo->name_lv }}}</td>
                            <td>{{{ $placeInfo->name_en }}}</td>
                            <td>{{{ $placeInfo->name_ru }}}</td>
                            <td>{{{ $placeInfo->name_est }}}</td>
                            <td align="center">
                                <i class="fa {{ (is_null($placeInfo->placeGallery) ? 'fa-picture-o text-red' : 'fa-check text-green') }}"></i>
                            </td>
                            <td align="center">
                                <i class="fa {{ (is_null($placeInfo->placeAudio) ? 'fa-phone text-red' : 'fa-check text-green') }}"></i>
                            </td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">Darbības</button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ route('update/place', $placeInfo->id) }}"><i class="fa fa-edit"></i> Labot</a></li>
                                        <li>
                                        <a href="{{ route('confirm-delete/place', $placeInfo->id) }}" data-toggle="modal" data-target="#delete_confirm"><i class="fa fa-trash-o"></i> Dzēst</a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    @else
                        <tr>
                            <td colspan="7">
                                Nav pievienots neviens vietas ieraksts
                            </td>
                        </tr>
                    @endif
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

@stop

{{-- Body Bottom confirm modal --}}
@section('body_bottom')
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="institution_delete_confirm_title" aria-hidden="true">
  <div class="modal-dialog" style="background-color:#ffffff;">
    <div class="modal-content" style="background-color:#ffffff;">
    </div>
  </div>
</div>
<script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
{{-- Make sortable places --}}
<script type="text/javascript">
function postPlaceWeight(data){
    $.ajax({
        type: 'POST',
        url: "{{ URL::to('admin/places/postOrder') }}",
        data: data,
        success: function(d){},
        dataType: 'json'
    });
    $("#placeTable").removeClass('table-striped').addClass('table-striped');
}
$(function(){
    $('#placeTable tbody').makeSortable({sortCallBack:'postPlaceWeight',handle:'.handle'});
});
</script>
@stop