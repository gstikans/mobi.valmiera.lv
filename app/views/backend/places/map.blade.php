@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Vietas kartē ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="row">
    <div class="col-xs-9 col-xs-offset-1">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title pull-left">Vietas kartē</h3>
                <div class="box-tools pull-right">
                    <a href="{{ URL::to('admin/places') }}" class="btn btn-info">
                        <i class="fa fa-arrow-left"></i> Vietu saraksts
                    </a>
                </div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <div id="mapBox" style="width:100%;height:550px"></div>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

@stop

{{-- Body Bottom confirm modal --}}
@section('body_bottom')
{{-- Sheit viss kas attiecas uz google map --}}
<script type="text/javascript">
var map;
var marker = null;
$(function () {
    function initMapBox(){
        var mapOptions = {
            center: new google.maps.LatLng(57.538466, 25.426362),
            zoom: 14,
            panControl: false,
            zoomControl: true,
            scaleControl: false,
            mapTypeControl: false,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        map = new google.maps.Map(document.getElementById("mapBox"),mapOptions);
        google.maps.event.addListenerOnce(map,'idle',function(){
            map.fitBounds(this.getBounds());
        });
    }
    initMapBox();
    //create markers
    @if(count($placeList) > 0)
    @foreach($placeList as $place)
        var marker_{{$place->id}} = new google.maps.Marker({
            position: new google.maps.LatLng({{ $place->latlng }}),
            map: map
        });
        var infoWindow_{{$place->id}} = new google.maps.InfoWindow({
            size: new google.maps.Size(250,250),
            content: '{{{ $place->name_lv }}}<br /><a href="{{ route('update/place', $place->id) }}"><i class="fa fa-edit"></i> Labot</a><br /><a href="{{ route('confirm-delete/place', $place->id) }}" data-toggle="modal" data-target="#delete_confirm"><i class="fa fa-trash-o"></i> Dzēst</a>'
        });
        google.maps.event.addListener(marker_{{$place->id}},'click',function(){
            infoWindow_{{$place->id}}.open(map,marker_{{$place->id}});
        });
    @endforeach
    @endif
});
</script>
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="busstop_delete_confirm_title" aria-hidden="true">
  <div class="modal-dialog" style="background-color:#ffffff;">
    <div class="modal-content" style="background-color:#ffffff;">
    </div>
  </div>
</div>
<script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
@stop