@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Labot vietas ierakstu ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="row">
    <div class="col-xs-8">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title pull-left">Labot vietas ierakstu</h3>
                <div class="box-tools pull-right"></div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">
                <form class="form-horizontal" role="form" method="post" action="">
                    <!-- CSRF Token -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                        <!-- Name -->
                        <div class="form-group {{ $errors->first('name', 'has-error') }}">
                            <label for="name" class="col-sm-3">Nosaukums</label>
                            <div class="col-sm-11">
                                <input type="text" id="name" name="name" class="form-control" placeholder="Nosaukums" value="{{{ Input::old('name',$restaurant->name) }}}">
                            </div>
                            <div class="col-sm-10">
                                {{ $errors->first('name', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>
                        <!-- Address -->
                        <div class="form-group {{ $errors->first('address', 'has-error') }}">
                            <label for="address" class="col-sm-3">Adrese</label>
                            <div class="col-sm-11">
                                <input type="text" id="address" name="address" class="form-control" placeholder="Adrese" value="{{{ Input::old('address',$restaurant->address) }}}">
                            </div>
                            <div class="col-sm-10">
                                {{ $errors->first('address', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>
                        <!-- Phone -->
                        <div class="form-group {{ $errors->first('phone', 'has-error') }}">
                            <label for="phone" class="col-sm-3">Telefons</label>
                            <div class="col-sm-11">
                                <input type="text" id="phone" name="phone" class="form-control" placeholder="Telefons" value="{{{ Input::old('phone',$restaurant->phone) }}}">
                            </div>
                            <div class="col-sm-10">
                                {{ $errors->first('phone', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>
                        <!-- Mail -->
                        <div class="form-group {{ $errors->first('mail', 'has-error') }}">
                            <label for="mail" class="col-sm-3">E-pasts</label>
                            <div class="col-sm-11">
                                <input type="text" id="mail" name="mail" class="form-control" placeholder="E-pasts" value="{{{ Input::old('mail',$restaurant->mail) }}}">
                            </div>
                            <div class="col-sm-10">
                                {{ $errors->first('mail', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>
                        <!-- Web -->
                        <div class="form-group {{ $errors->first('web', 'has-error') }}">
                            <label for="web" class="col-sm-3">Mājaslapa</label>
                            <div class="col-sm-11">
                                <input type="text" id="web" name="web" class="form-control" placeholder="Mājaslapa" value="{{{ Input::old('web',$restaurant->web) }}}">
                            </div>
                            <div class="col-sm-10">
                                {{ $errors->first('web', '<span class="help-block">:message</span>') }}
                            </div>
                        </div>
                    <!-- Form Actions -->
                    <div class="form-group" style="margin-top:10px;">
                        <div class="col-xs-offset-8 col-xs-4">
                            <a class="btn btn-warning" href="{{ route('restaurants') }}">@lang('button.cancel')</a>
                            <button type="submit" class="btn btn-success">@lang('button.save')</button>
                        </div>
                    </div>
                </form>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
</div>

@stop