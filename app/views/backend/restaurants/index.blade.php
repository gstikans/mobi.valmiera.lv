@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Vietas maltītēm ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="row">
    <div class="col-xs-8">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title pull-left">Vietas maltītēm</h3>
                <div class="box-tools pull-right"></div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nosaukums</th>
                            <th>Adrese</th>
                            <th>Telefons</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($restaurantList)>0)
                            @foreach($restaurantList as $restaurantInfo)
                                <tr>
                                    <td>{{{ $restaurantInfo->name }}}</td>
                                    <td>{{{ $restaurantInfo->address }}}</td>
                                    <td>{{{ $restaurantInfo->phone }}}</td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">Darbības</button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li><a href="{{ route('update/restaurant', $restaurantInfo->id) }}"><span class="glyphicon glyphicon-pencil"></span> Labot</a></li>
                                                <li>
                                                <a href="{{ route('confirm-delete/restaurant', $restaurantInfo->id) }}" data-toggle="modal" data-target="#delete_confirm"><span class="glyphicon glyphicon-trash"></span> Dzēst</a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        @else
                            <tr>
                                <td colspan="4">Nav pievienots neviens ieraksts</td>
                            </tr>
                        @endif
                    </tbody>
                </table>
            </div><!-- /.box-body -->
        </div><!-- /.box -->
    </div>
    <!-- user form -->
    <div class="col-xs-4">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Jauna vieta</h3>
                <div class="box-tools"></div>
            </div><!-- /.box-header -->
            <div class="box-body">
            	@include('backend/restaurants/create')
            </div>
        </div>
    </div>
</div>

@stop

{{-- Body Bottom confirm modal --}}
@section('body_bottom')
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="restaurant_delete_confirm_title" aria-hidden="true">
  <div class="modal-dialog" style="background-color:#ffffff;">
    <div class="modal-content" style="background-color:#ffffff;">
    </div>
  </div>
</div>
<script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>

@stop