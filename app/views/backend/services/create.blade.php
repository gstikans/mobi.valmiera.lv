<form class="form-horizontal" role="form" method="post" action="{{ URL::to('admin/services/create') }}">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <!-- Name -->
        <div class="form-group {{ $errors->first('name', 'has-error') }}">
            <!--<label for="name" class="col-sm-2 control-label">Nosaukums</label>-->
                <div class="col-sm-12">
                    <input type="text" id="name" name="name" class="form-control" placeholder="Nosaukums" value="{{{ Input::old('name') }}}">
                </div>
                <div class="col-sm-7">
                    {{ $errors->first('name', '<span class="help-block">:message</span>') }}
                </div>
        </div>
        <!-- Address -->
        <div class="form-group {{ $errors->first('address', 'has-error') }}">
            <!--<label for="address" class="col-sm-2 control-label">Adrese</label>-->
                <div class="col-sm-12">
                    <input type="text" id="address" name="address" class="form-control" placeholder="Adrese" value="{{{ Input::old('address') }}}">
                </div>
                <div class="col-sm-7">
                    {{ $errors->first('address', '<span class="help-block">:message</span>') }}
                </div>
        </div>
        <!-- Phone -->
        <div class="form-group {{ $errors->first('phone', 'has-error') }}">
            <!-- <label for="phone" class="col-sm-2 control-label">Telefons</label>-->
                <div class="col-sm-12">
                    <input type="text" id="phone" name="phone" class="form-control" placeholder="Telefons" value="{{{ Input::old('phone') }}}">
                </div>
                <div class="col-sm-7">
                    {{ $errors->first('phone', '<span class="help-block">:message</span>') }}
                </div>
        </div>
        <!-- Mail -->
        <div class="form-group {{ $errors->first('mail', 'has-error') }}">
            <!--<label for="mail" class="col-sm-2 control-label">E-pasts</label>-->
                <div class="col-sm-12">
                    <input type="text" id="mail" name="mail" class="form-control" placeholder="E-pasts" value="{{{ Input::old('mail') }}}">
                </div>
                <div class="col-sm-7">
                    {{ $errors->first('mail', '<span class="help-block">:message</span>') }}
                </div>
        </div>
        <!-- description -->
        <div class="form-group {{ $errors->first('mail', 'has-error') }}">
            <!--<label for="mail" class="col-sm-2 control-label">Apraksts</label>-->
                <div class="col-sm-12">
                    <textarea class="form-control" rows="3" name="description" placeholder="Apraksts">{{{ Input::old('description') }}}</textarea>
                </div>
                <div class="col-sm-7">
                    {{ $errors->first('description', '<span class="help-block">:message</span>') }}
                </div>
        </div>
    <!-- Form Actions -->
    <div class="form-group" style="margin-top:10px;">
        <div class="col-xs-offset-5 col-xs-7">
            <a class="btn btn-warning" href="{{ route('services') }}">@lang('button.cancel')</a>
            <button type="submit" class="btn btn-success">@lang('button.save')</button>
        </div>
    </div>
</form>
