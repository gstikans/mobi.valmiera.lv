@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Dienesta informācijas atjaunināšana ::
@parent
@stop

{{-- Page content --}}
@section('content')

<div class="col-xs-6">
    <div class="box box-primary">
        <div class="box-header">
            <h3 class="box-title">Dienesta informācijas atjaunināšana</h3>
        </div>
        <form class="form-horizontal" role="form" method="post" action="">
            <div class="box-body">
            <!-- CSRF Token -->
            <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                <!-- Name -->
                <div class="form-group {{ $errors->first('name', 'has-error') }}">
                    <label for="name" class="col-sm-2 control-label">Nosaukums</label>
                        <div class="col-sm-10">
                            <input type="text" id="name" name="name" class="form-control" placeholder="Nosaukums" value="{{{ Input::old('name',$service->name) }}}">
                        </div>
                        <div class="col-sm-7 col-sm-offset-2">
                            {{ $errors->first('name', '<span class="help-block">:message</span>') }}
                        </div>
                </div>
                <!-- Address -->
                <div class="form-group {{ $errors->first('address', 'has-error') }}">
                    <label for="address" class="col-sm-2 control-label">Adrese</label>
                        <div class="col-sm-10">
                            <input type="text" id="address" name="address" class="form-control" placeholder="Adrese" value="{{{ Input::old('address',$service->address) }}}">
                        </div>
                        <div class="col-sm-7 col-sm-offset-2">
                            {{ $errors->first('address', '<span class="help-block">:message</span>') }}
                        </div>
                </div>
                <!-- Phone -->
                <div class="form-group {{ $errors->first('phone', 'has-error') }}">
                    <label for="phone" class="col-sm-2 control-label">Telefons</label>
                        <div class="col-sm-10">
                            <input type="text" id="phone" name="phone" class="form-control" placeholder="Telefons" value="{{{ Input::old('phone',$service->phone) }}}">
                        </div>
                        <div class="col-sm-7 col-sm-offset-2">
                            {{ $errors->first('phone', '<span class="help-block">:message</span>') }}
                        </div>
                </div>
                <!-- Mail -->
                <div class="form-group {{ $errors->first('mail', 'has-error') }}">
                    <label for="mail" class="col-sm-2 control-label">E-pasts</label>
                        <div class="col-sm-10">
                            <input type="text" id="mail" name="mail" class="form-control" placeholder="E-pasts" value="{{{ Input::old('mail',$service->mail) }}}">
                        </div>
                        <div class="col-sm-7 col-sm-offset-2">
                            {{ $errors->first('mail', '<span class="help-block">:message</span>') }}
                        </div>
                </div>
                <!-- description -->
                <div class="form-group {{ $errors->first('mail', 'has-error') }}">
                    <label for="mail" class="col-sm-2 control-label">Apraksts</label>
                        <div class="col-sm-10">
                            <textarea class="form-control" rows="5" name="description" placeholder="Apraksts">{{{ Input::old('description',$service->description) }}}</textarea>
                        </div>
                        <div class="col-sm-7 col-sm-offset-2">
                            {{ $errors->first('description', '<span class="help-block">:message</span>') }}
                        </div>
                </div>
            <!-- Form Actions -->
            </div>
            <div class="box-footer">
                <div class="form-group" style="margin-top:10px;">
                    <div class="col-xs-offset-8 col-xs-4">
                        <a class="btn btn-warning" href="{{ route('services') }}">@lang('button.cancel')</a>
                        @if(Sentry::check() && Sentry::getUser()->hasAccess('services.create'))
                        <button type="submit" class="btn btn-success">@lang('button.save')</button>
                        @endif
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
@stop