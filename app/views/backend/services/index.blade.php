@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Dienestu pārvaldība ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="row">
    <div class="col-xs-8">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Dienestu saraksts</h3>
                <div class="box-tools"></div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>Nosaukums</th>
                            <th>Adrese</th>
                            <th>Telefons</th>
                            <th>E-pasts</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(count($services))
                        @foreach ($services as $service)
                        <tr>
                            <td>{{{ $service->name }}}</td>
                            <td>{{{ $service->address }}}</td>
                            <td>{{{ $service->phone }}}</td>
                            <td>{{{ $service->mail }}}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">Darbības</button>
                                    <ul class="dropdown-menu" role="menu">
                                        @if(Sentry::check() && Sentry::getUser()->hasAccess('services.create'))
                                        <li><a href="{{ route('update/service', $service->id) }}"><span class="glyphicon glyphicon-pencil"></span> Labot</a></li>
                                        @else
                                           <li><a href="{{ route('update/service', $service->id) }}"><span class="glyphicon glyphicon-pencil"></span> Info</a></li> 
                                        @endif
                                        @if(Sentry::check() && Sentry::getUser()->hasAccess('services.create'))
                                        <li>
                                        <a href="{{ route('confirm-delete/service', $service->id) }}" data-toggle="modal" data-target="#delete_confirm"><span class="glyphicon glyphicon-trash"></span> Dzēst</a></li>
                                        @endif
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                        @else
                        <tr>
                            <td colspan="4">
                                Nav pievienots neviens dienests
                            </td>
                        </td>
                        @endif
                    </tbody>
                </table>
            </div><!-- /.box-body -->
            <div class="box-footer">
                @if (count($services))
                {{ $services->links() }}
                @endif
            </div>
        </div><!-- /.box -->
    </div>
    <!-- user form -->
    <div class="col-xs-4">
        @if(Sentry::check() && Sentry::getUser()->hasAccess('services.create'))
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Jauns dienests</h3>
                <div class="box-tools"></div>
            </div><!-- /.box-header -->
            <div class="box-body">
                @include('backend/services/create')
            </div>
        </div>
        @endif
    </div>
</div>

@stop

{{-- Body Bottom confirm modal --}}
@section('body_bottom')
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="service_delete_confirm_title" aria-hidden="true">
  <div class="modal-dialog" style="background-color:#ffffff;">
    <div class="modal-content" style="background-color:#ffffff;">
    </div>
  </div>
</div>
<script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
@stop
