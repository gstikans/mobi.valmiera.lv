<form class="form-horizontal" role="form" method="post" action="{{ URL::to('admin/taxi/create') }}">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
        <!-- Name -->
        <div class="form-group {{ $errors->first('name', 'has-error') }}">
            <!--<label for="name" class="col-sm-3">Nosaukums</label>-->
                <div class="col-sm-11">
                    <input type="text" id="name" name="name" class="form-control" placeholder="Nosaukums" value="{{{ Input::old('name') }}}">
                </div>
                <div class="col-sm-10">
                    {{ $errors->first('name', '<span class="help-block">:message</span>') }}
                </div>
        </div>
        <!-- Address -->
        <div class="form-group {{ $errors->first('address', 'has-error') }}">
            <!--<label for="address" class="col-sm-3">Adrese</label>-->
                <div class="col-sm-11">
                    <input type="text" id="address" name="address" class="form-control" placeholder="Adrese" value="{{{ Input::old('address') }}}">
                </div>
                <div class="col-sm-10">
                    {{ $errors->first('address', '<span class="help-block">:message</span>') }}
                </div>
        </div>
        <!-- Address -->
        <div class="form-group {{ $errors->first('phone', 'has-error') }}">
            <!--<label for="phone" class="col-sm-3">Telefons</label>-->
                <div class="col-sm-11">
                    <input type="text" id="phone" name="phone" class="form-control" placeholder="Telefons" value="{{{ Input::old('phone') }}}">
                </div>
                <div class="col-sm-10">
                    {{ $errors->first('phone', '<span class="help-block">:message</span>') }}
                </div>
        </div>
    <!-- Form Actions -->
    <div class="form-group" style="margin-top:10px;">
        <div class="col-xs-offset-6 col-xs-6">
            <a class="btn btn-warning" href="{{ route('taxi') }}">@lang('button.cancel')</a>
            <button type="submit" class="btn btn-success">@lang('button.save')</button>
        </div>
    </div>
</form>