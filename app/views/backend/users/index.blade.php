@extends('backend/layouts/default')

{{-- Page title --}}
@section('title')
Lietotāju pārvaldība ::
@parent
@stop

{{-- Page content --}}
@section('content')
<div class="row">
    <div class="col-xs-6">
        <div class="box">
            <div class="box-header">
                <h3 class="box-title">Lietotāju saraksts</h3>
                <div class="box-tools"></div>
            </div><!-- /.box-header -->
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <thead>
                        <tr>
                            <th>@lang('admin/users/table.first_name')</th>
                            <th>@lang('admin/users/table.last_name')</th>
                            <th>@lang('admin/users/table.email')</th>
                            <th>@lang('admin/users/table.activated')</th>
                            <th>@lang('admin/users/table.created_at')</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($users as $user)
                        <tr
                        @if ($user->accountStatus()=='suspended')
                                <?php echo ' class="warning"'; ?>
                            @elseif ($user->accountStatus()=='banned')
                                <?php echo ' class="danger"'; ?>
                            @endif>
                            <td>{{{ $user->first_name }}}</td>
                            <td>{{{ $user->last_name }}}</td>
                            <td>{{{ $user->email }}}</td>
                            <td>@lang('general.' . ($user->isActivated() ? 'yes' : 'no'))</td>
                            <td>{{{ $user->created_at->diffForHumans() }}}</td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-info btn-sm dropdown-toggle" data-toggle="dropdown">Darbības</button>
                                    <ul class="dropdown-menu" role="menu">
                                        <li><a href="{{ route('update/user', $user->id) }}"><span class="glyphicon glyphicon-pencil"></span> Labot</a></li>
                                        <li>@if (Sentry::getId() !== $user->id)
                                        <a href="{{ route('confirm-delete/user', $user->id) }}" data-toggle="modal" data-target="#delete_confirm"><span class="glyphicon glyphicon-trash"></span> Dzēst</a>
                                    @else
                                        <a href="#">
                                            <span class="glyphicon glyphicon-trash text-muted"></span> Dzēst
                                        </a>
                                    @endif</li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div><!-- /.box-body -->
            <div class="box-footer">
                @if (count($users))
                {{ $users->links() }}
                @endif
            </div>
        </div><!-- /.box -->
    </div>
    <!-- user form -->
    <div class="col-xs-6">
        <div class="box box-primary">
            <div class="box-header">
                <h3 class="box-title">Jauns lietotājs</h3>
                <div class="box-tools"></div>
            </div><!-- /.box-header -->
            <div class="box-body">
                @include('backend/users/create',array('groups'=>$groups,'selectedGroups'=>$selectedGroups,'permissions'=>$permissions,'selectedPermissions'=>$selectedPermissions))
            </div>
        </div>
    </div>
</div>

@stop

{{-- Body Bottom confirm modal --}}
@section('body_bottom')
<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
  <div class="modal-dialog" style="background-color:#ffffff;">
    <div class="modal-content" style="background-color:#ffffff;">
    </div>
  </div>
</div>
<script>$(function () {$('body').on('hidden.bs.modal', '.modal', function () {$(this).removeData('bs.modal');});});</script>
@stop
