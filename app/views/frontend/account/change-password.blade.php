@extends('frontend/layouts/account')

{{-- Page title --}}
@section('title')
Labot lietotāja profilu ::
@parent
@stop

{{-- Account page content --}}
@section('account-content')

<form class="form-horizontal" role="form" method="post" action="" autocomplete="off">
    <!-- CSRF Token -->
    <input type="hidden" name="_token" value="{{ csrf_token() }}" />

    <!-- Old Password -->
    <!-- Confirm Password -->
    <div class="form-group {{ $errors->first('old_password', 'has-error') }}">
        <label for="old_password" class="col-sm-3 control-label">Esošā parole</label>
            <div class="col-sm-5">
                <input type="password" id="old_password" name="old_password" class="form-control">
            </div>
            <div class="col-sm-4">
                {{ $errors->first('old_password', '<span class="help-block">:message</span>') }}
            </div>
    </div>

    <!-- New Password -->
    <div class="form-group {{ $errors->first('password', 'has-error') }}">
        <label for="password" class="col-sm-3 control-label">Jaunā parole</label>
            <div class="col-sm-5">
                <input type="password" id="password" name="password" class="form-control">
            </div>
            <div class="col-sm-4">
                {{ $errors->first('password', '<span class="help-block">:message</span>') }}
            </div>
    </div>

    <!-- Confirm New Password  -->
    <div class="form-group {{ $errors->first('password_confirm', 'has-error') }}">
        <label for="password_confirm" class="col-sm-3 control-label">Apstiprināt paroli</label>
            <div class="col-sm-5">
                <input type="password" id="password_confirm" name="password_confirm" class="form-control">
            </div>
            <div class="col-sm-4">
                {{ $errors->first('password_confirm', '<span class="help-block">:message</span>') }}
            </div>
    </div>

    <hr>

    <!-- Form actions -->
    <div class="control-group">
        <div class="controls">
            <button type="submit" class="btn btn-default">@lang('button.update')</button>
        </div>
    </div>
</form>
@stop
