<!DOCTYPE html>
<html class="bg-black">
    <head>
        <meta charset="UTF-8">
        <title>Pieslēgties administrācijas panelim</title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/AdminLTE.css') }}" rel="stylesheet">

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif]-->
    </head>
    <body class="bg-black">
        <!-- Notifications -->
        <div class="row" style="margin-top:20px;">
            <div class="col-xs-6 col-xs-offset-3">
                @include('frontend/notifications')
            </div>
        </div>
        <!-- login form -->
        <div class="form-box" id="login-box" style="margin-top:40px;">
            <div class="header">Pieslēgties</div>
            <form role="form" method="post" action="{{ route('signin') }}">
                <div class="body bg-gray">
                    <!-- CSRF Token -->
                    <input type="hidden" name="_token" value="{{ csrf_token() }}" />
                    <!-- Email -->
                    <div class="form-group {{ $errors->first('email', 'has-error') }}">
                        <input type="email" class="form-control" name="email" id="email" value="{{ Input::old('email') }}" placeholder="E-pasts">
                        <div class="col-sm-4">
                            {{ $errors->first('email', '<span class="help-block">:message</span>') }}
                        </div>
                    </div>
                    <!-- Password -->
                    <div class="form-group {{ $errors->first('password', 'has-error') }}">
                        <input type="password" class="form-control" name="password" id="password">
                        <div class="col-sm-4">
                            {{ $errors->first('password', '<span class="help-block">:message</span>') }}
                        </div>
                    </div>         
                </div>
                <div class="footer">                                                               
                    <button type="submit" class="btn bg-olive btn-block">Pieslēgties</button>  
                </div>
            </form>

            <div class="margin text-center">
                <span>ValmieraInfo administrācijas panelis</span>
            </div>
        </div>


        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <!-- Bootstrap -->
        <script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>       

    </body>
</html>

