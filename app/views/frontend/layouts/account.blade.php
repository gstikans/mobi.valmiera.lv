@extends('frontend/layouts/default')

{{-- Page content --}}
@section('content')

<div class="row">
    <div class="col-sm-8 col-sm-offset-2">
    	<div class="box box-primary">
            <div class="box-header">
            </div><!-- /.box-header -->
            <div class="box-body">
            	<div class="nav-tabs-custom">
	            	<ul class="nav nav-tabs">
			            <li{{ Request::is('account/profile') ? ' class="active"' : '' }}><a href="{{ URL::route('profile') }}">Profils</a></li>
			            <li{{ Request::is('account/change-password') ? ' class="active"' : '' }}><a href="{{ URL::route('change-password') }}">Mainīt paroli</a></li>
			            <li{{ Request::is('account/change-email') ? ' class="active"' : '' }}><a href="{{ URL::route('change-email') }}">Mainīt e-pastu</a></li>
			        </ul>
		    	</div>
                @yield('account-content')
            </div>
        </div>
    </div>
</div>
@stop
