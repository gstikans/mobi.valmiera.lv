<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8" />
        <title>
            @section('title')
                @lang('general.site_name')
            @show
        </title>
        <meta name="keywords" content="your, awesome, keywords, here" />
        <meta name="author" content="Jon Doe" />
        <meta name="description" content="Lorem ipsum dolor sit amet, nihil fabulas et sea, nam posse menandri scripserit no, mei." />

        <!-- Mobile Specific Metas
        ================================================== -->
        <meta name="viewport" content="width=device-width, initial-scale=1.0">

        <!-- CSS
        ================================================== -->
        <link href="{{ asset('assets/css/bootstrap.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/font-awesome.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/ionicons.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/morris/morris.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/jvectormap/jquery-jvectormap-1.2.2.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/fullcalendar/fullcalendar.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css') }}" rel="stylesheet">
        <link href="{{ asset('assets/css/AdminLTE.css') }}" rel="stylesheet"> 
        <!--
        <style>
        @section('styles')
        @show
        </style> -->

        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
          <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
        <![endif] -->
    </head>

    <body class="skin-blue">
        <!-- new style -->
        <!-- header logo: style can be found in header.less -->
        <header class="header">
            <a href="/" class="logo">
                <!-- Add the class icon to your logo image or logo icon to add the margining -->
                @lang('general.site_name')
            </a>
            <!-- Header Navbar: style can be found in header.less -->
            <nav class="navbar navbar-static-top" role="navigation">
                <!-- Sidebar toggle button-->
                <a href="#" class="navbar-btn sidebar-toggle" data-toggle="offcanvas" role="button">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </a>
                <div class="navbar-right">
                    <ul class="nav navbar-nav">
                        @if(Sentry::check())
                        <!-- User Account: style can be found in dropdown.less -->
                        <li class="dropdown user user-menu">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="glyphicon glyphicon-user"></i>
                                <span>{{ Sentry::getUser()->first_name }} {{ Sentry::getUser()->last_name }} <i class="caret"></i></span>
                            </a>
                            <ul class="dropdown-menu">
                                <!-- User image -->
                                <li class="user-header bg-light-blue">
                                    <p>
                                        {{ Sentry::getUser()->first_name }} {{ Sentry::getUser()->last_name }}
                                        <small>Izveidots: {{ Sentry::getUser()->created_at }}</small>
                                    </p>
                                </li>
                                <!-- Menu Footer-->
                                <li class="user-footer">
                                    <div class="pull-left">
                                        <a href="{{ route('profile') }}" class="btn btn-default btn-flat">Profils</a>
                                    </div>
                                    <div class="pull-right">
                                        <a href="{{ route('logout') }}" class="btn btn-default btn-flat">Izlogoties</a>
                                    </div>
                                </li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </div>
            </nav>
        </header>
        <div class="wrapper row-offcanvas row-offcanvas-left">
            <!-- Left side column. contains the logo and sidebar -->
            <aside class="left-side sidebar-offcanvas">
                <!-- sidebar: style can be found in sidebar.less -->
                <section class="sidebar">
                    @if(Sentry::check())
                    <!-- Sidebar user panel -->
                    <div class="user-panel">
                        <div class="pull-left info">
                            <p>Sveiki, {{Sentry::getUser()->first_name }} {{ Sentry::getUser()->last_name}}</p>

                            <a href="#"><i class="fa fa-circle text-success"></i> Tiešsaistē</a>
                        </div>
                    </div>
                    @endif
                    <!-- sidebar menu: : style can be found in sidebar.less -->
                    <ul class="sidebar-menu">
                        <li class="{{ (Request::is('admin') ? 'active' : '') }}">
                            <a href="/admin">
                                <i class="fa fa-dashboard"></i> <span>Darbavirsma</span>
                            </a>
                        </li>
                        @if(Sentry::check() && Sentry::getUser()->hasAccess('places.read'))
                        <li class="{{ (Request::is('admin/places*') ? 'active' : '') }}">
                            <a href="{{ URL::to('admin/places') }}">
                                <i class="fa fa-globe"></i> Vietas
                            </a>
                        </li>
                        @endif
                        @if(Sentry::check() && Sentry::getUser()->hasAccess('audioguide.read'))
                        <li class="{{ (Request::is('admin/audioguides*') ? 'active' : '') }}">
                            <a href="{{ URL::to('admin/audioguides') }}">
                                <i class="fa fa-phone"></i> Audiogids
                            </a>
                        </li>
                        @endif
                        @if(Sentry::check() && Sentry::getUser()->hasAccess('services.read'))
                        <li class="{{ (Request::is('admin/services*') ? 'active' : '') }}">
                            <a href="{{ URL::to('admin/services') }}">
                                <i class="fa fa-gear"></i> Dienesti
                            </a>
                        </li>
                        @endif
                        @if(Sentry::check() && Sentry::getUser()->hasAccess('municipality.read'))
                        <li class="{{ (Request::is('admin/institutions*') ? 'active' : '') }}">
                            <a href="{{ URL::to('admin/institutions') }}">
                                <i class="fa fa-building-o"></i> Pašvaldība
                            </a>
                        </li>
                        @endif
                        @if(Sentry::check() && Sentry::getUser()->hasAccess('taxi.read'))
                        <li class="{{ (Request::is('admin/taxi*') ? 'active' : '') }}">
                            <a href="{{ URL::to('admin/taxi') }}">
                                <i class="fa fa-truck"></i> Taksometri
                            </a>
                        </li>
                        @endif
                        @if(Sentry::check() && Sentry::getUser()->hasAccess('restaurants.read'))
                        <li class="{{ (Request::is('admin/restaurants*') ? 'active' : '') }}">
                            <a href="{{ URL::to('admin/restaurants') }}">
                                <i class="fa fa-cutlery"></i> Kur paēst?
                            </a>
                        </li>
                        @endif
                        @if(Sentry::check() && Sentry::getUser()->hasAccess('hotels.read'))
                        <li class="{{ (Request::is('admin/hotels*') ? 'active' : '') }}">
                            <a href="{{ URL::to('admin/hotels') }}">
                                <i class="fa fa-home"></i> Naktsmītnes
                            </a>
                        </li>
                        @endif
                        @if(Sentry::check() && Sentry::getUser()->hasAccess('busstops.read'))
                        <li class="{{ (Request::is('admin/busstops*') ? 'active' : '') }}">
                            <a href="{{ URL::to('admin/busstops') }}">
                                <i class="fa fa-road"></i> Pieturas
                            </a>
                        </li>
                        @endif
                        <!-- menu kuriem vajag superadmin -->
                        @if(Sentry::check() && Sentry::getUser()->hasAccess('superadmin'))
                        <li class="treeview {{ (Request::is('admin/users*|admin/groups*') ? ' active' : '') }}">
                            <a href="#">
                                <i class="fa fa-users"></i>
                                <span>Lietotāji un Grupas</span>
                                <i class="fa fa-angle-left pull-right"></i>
                            </a>
                            <ul class="treeview-menu">
                                <li{{ (Request::is('admin/users*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/users') }}"><i class="icon-user"></i> Lietotāji</a></li>
                                <li{{ (Request::is('admin/groups*') ? ' class="active"' : '') }}><a href="{{ URL::to('admin/groups') }}"><i class="icon-user"></i> Grupas</a></li>
                            </ul>
                        </li>
                        @endif
                    </ul>
                </section>
                <!-- /.sidebar -->
            </aside>

            <!-- Right side column. Contains the navbar and content of the page -->
            <aside class="right-side">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>
                        <small>
                        <b>@section('title')</b>
                        @show
                        </small>
                    </h1>
                </section>

                <!-- Main content -->
                <section class="content" style="padding-bottom:50px;">

                    <!-- Main row -->
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- Notifications -->
                            @include('frontend/notifications')
                            <!-- Content -->
                            @yield('content')
                        </div>

                    </div><!-- /.row (main row) -->

                </section><!-- /.content -->
            </aside><!-- /.right-side -->
        </div><!-- ./wrapper -->

        <!-- jQuery 2.0.2 -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBKo8B-ljeGMsk-WqIkDKMnS1LTb--PCVI"></script>
        <script src="{{ asset('assets/js/jquery-ui-1.10.3.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/plugins/sparkline/jquery.sparkline.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/plugins/fullcalendar/fullcalendar.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/plugins/jqueryKnob/jquery.knob.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/plugins/daterangepicker/daterangepicker.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('assets/js/AdminLTE/app.js') }}" type="text/javascript"></script>

        @section('body_bottom')
        @show
    </body>
</html>
